<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class Queue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;

    /**
     * @see https://blog.csdn.net/rockywish/article/details/106854134
     *
     * Queue constructor.
     * @param $data
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @return false|string
     */
    public function handle()
    {
        Log::info('Handle: ' . date('Y-m-d H:i:s', time()));
        sleep(2);
        Log::info('Params: ' . json_encode($this->data));
        return json_encode($this->data);
    }
}
