<?php

namespace App\Console\Commands\Lib;


use Carbon\Carbon;

/**
 * 需要一个时间段的命令计划使用
 * Trait DataDate
 * @package App\Console\Commands\Lib
 */
trait ByPeriod
{
    /**
     * @var Carbon
     */
    private $start;

    /**
     * @var Carbon
     */
    private $end;

    /**
     * 初始化时间段
     */
    private function initPeriod()
    {
        // 传参优先
        $start = call_user_func([$this, 'argument'], 'start');
        $end = call_user_func([$this, 'argument'], 'end');

        $this->start = Carbon::parse($start);
        $this->end = Carbon::parse($end);
    }

    /**
     * 获取时间段
     * @param bool $timestamp
     * @return mixed
     */
    protected function getPeriod($timestamp = false)
    {
        if (!$this->start) {
            $this->initPeriod();
        }
        return $timestamp ? [$this->start->timestamp, $this->end->timestamp] : [(string)$this->start, (string)$this->end];
    }
}
