<?php

namespace App\Services\Upload;

use App\Exceptions\RJsonError;
use OSS\Core\OssException;
use OSS\OssClient;

/**
 * @see https://blog.csdn.net/ivneptune/article/details/110632181
 * 
 * @author ChenGuangHui
 * @dateTime 2022-06-24
 */
class AliyunService
{

    /**
     * 初始化函数
     * 
     * @author ChenGuangHui
     * @dateTime 2022-06-24
     */
    public function __construct()
    {
        $bucket = config('config.aliyun.bucket');
        try {
            $ossClient = new OssClient(config('config.aliyun.access_key'), config('config.aliyun.access_secret'), config('config.aliyun.endpoint'));
            $uploadId = $ossClient->initiateMultipartUpload($bucket, trim($attributes['file_path'], '/') . '/' . $attributes['original_name']);
        } catch (OssException $e) {
            throw new RJsonError('阿里云OSS配置错误！');
        }
    }

    /**
     * 
     * 把本地变量的内容到文件
     * 简单上传,上传指定变量的内存值作为object的内容
     * 
     * @author ChenGuangHui
     * @dateTime 2022-06-24
     * @param [type] $imgPath
     * @param [type] $object
     */
    public function putObject($imgPath, $object)
    {
        $content = file_get_contents($imgPath); // 把当前文件的内容获取到传入文件中
        $options = array();
        try {
            $this->ossClient->putObject($this->bucket, $object, $content, $options);
        } catch (OssException $e) {
            return $e->getMessage();
        }
        return TRUE;
    }

    /**
     * 上传指定的本地文件内容
     * 
     * @author ChenGuangHui
     * @dateTime 2022-06-24
     * @param [type] $imgPath
     * @param [type] $object
     */
    public function uploadFile($imgPath, $object) //$_FILES['img']['tmp_name']
    {
        $filePath = $imgPath;
        $options = array();
        try {
            $this->ossClient->uploadFile($this->bucket, $object, $filePath, $options);
        } catch (OssException $e) {
            return $e->getMessage();
        }
        return TRUE;
    }

    /**
     * 删除对象
     * 
     * @author ChenGuangHui
     * @dateTime 2022-06-24
     * @param [type] $object
     */
    public function deleteObject($object)
    {
        try {
            $this->ossClient->deleteObject($this->bucket, $object);
        } catch (OssException $e) {
            return $e->getMessage();
        }
        return TRUE;
    }

    /**
     * 判断对象是否存在
     * 
     * @author ChenGuangHui
     * @dateTime 2022-06-24
     * @param [type] $object
     */
    public function doesObjectExist($object)
    {
        try {
            $result = $this->ossClient->doesObjectExist($this->bucket, $object);
        } catch (OssException $e) {
            return $e->getMessage();
        }
        return $result;
    }

    /**
     * 批量删除对象
     * 
     * @author ChenGuangHui
     * @dateTime 2022-06-24
     * @param [type] $objects
     */
    public function deleteObjects($objects)
    {
        try {
            $this->ossClient->deleteObjects($this->bucket, $objects);
        } catch (OssException $e) {
            return $e->getMessage();
        }
        return TRUE;
    }

    /**
     * 获取object的内容
     *
     * @param OssClient $ossClient OssClient实例
     * @param string $bucket 存储空间名称
     * @return null
     */
    public function getObject($object)
    {
        $options = array();
        try {
            $content = $this->ossClient->getObject($this->bucket, $object, $options);
        } catch (OssException $e) {
            return $e->getMessage();
        }
        return $content;
    }
}
