<?php
/*
 * @author: ChenGuangHui
 */
/*
 * @author: ChenGuangHui
 */
/*
 * @author: ChenGuangHui
 */
namespace App\Services\Payment;

use App\Services\Payment\PayFactory;
use EasyWeChat\Factory;

class WechatService extends PayFactory
{
    private static $instance = null;
 
    // 禁止被实例化
    private function __construct()
    {
 
    }
 
    // 禁止clone
    private function __clone()
    {
 
    }

    /**
     * 单例
     * 
     * @author ChenGuangHui
     * @dateTime 2022-06-14
     * @return object
     */
    public static function getInstance($order): object
    {
        if (empty(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * 检查订单
     * 
     * @author ChenGuangHui
     * @dateTime 2022-06-14
     * @param [type] $data
     */
    public function check()
    {

        return $this;
    }
 
    /**
     * @author ChenGuangHui
     * @dateTime 2022-06-14
     */
    public function refund()
    {
        $config = [
            // 必要配置
            'app_id'             => 'xxxx',
            'mch_id'             => 'your-mch-id',
            'key'                => 'key-for-signature',   // API v2 密钥 (注意: 是v2密钥 是v2密钥 是v2密钥)
        
            // 如需使用敏感接口（如退款、发送红包等）需要配置 API 证书路径(登录商户平台下载 API 证书)
            'cert_path'          => 'path/to/your/cert.pem', // XXX: 绝对路径！！！！
            'key_path'           => 'path/to/your/key',      // XXX: 绝对路径！！！！
        
            'notify_url'         => '默认的订单回调地址',     // 你也可以在下单时单独设置来想覆盖它
        ];
        
        $app = Factory::payment($config);
        
        // 参数分别为：微信订单号、商户退款单号、订单金额、退款金额、其他参数
        $result = $app->refund->byTransactionId('transaction-id-xxx', 'refund-no-xxx', 10000, 10000, [
            // 可在此处传入其他参数，详细参数见微信支付文档
            'refund_desc' => 'xx',
        ]);
        return array_merge($result, [
            'order' => $this->order
        ]);
    }
}