<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/9/9 15:49
 */


namespace App\Facades;


use Illuminate\Support\Facades\Facade;

class Oss extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'App\Services\AliyunService';
    }
}

