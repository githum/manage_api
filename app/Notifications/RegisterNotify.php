<?php

namespace App\Notifications;

use App\Mail\RegisterMail;
use App\Models\Staffer\Staffer;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Leonis\Notifications\EasySms\Channels\EasySmsChannel;
use Leonis\Notifications\EasySms\Messages\EasySmsMessage;

class RegisterNotify extends Notification
{
    use Queueable;

    public $staffer;

    /**
     * Create a new notification instance.
     *
     * @param Staffer $staffer
     */
    public function __construct(Staffer $staffer)
    {
        $this->staffer = $staffer;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via(Staffer $notifiable)
    {
        $via = ['database'];

        if ($notifiable->email) {
            $via[] = 'mail';
        }

        // if ($notifiable->mobile) {
        //     $via[] = [EasySmsChannel::class];
        // }

        return $via;
    }

    /**
     * @param $notifiable
     * @return EasySmsMessage
     */
    public function toEasySms($notifiable)
    {
        return (new EasySmsMessage)
            ->setContent('您的验证码为: 6379')
            ->setTemplate('SMS_001')
            ->setData(['code' => 6379]);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return RegisterMail
     */
    public function toMail($notifiable)
    {
        return (new RegisterMail($this->staffer))->to($notifiable->email);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
