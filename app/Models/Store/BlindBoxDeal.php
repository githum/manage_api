<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2022/04/13 11:00
 */

namespace App\Models\Store;

use App\Models\Model;

class BlindBoxDeal extends Model
{
    
    protected $connection = 'store';

    protected $table = 'store_blind_box_deal';

    protected $primaryKey = 'id';
    
    protected $guarded = [];
}
