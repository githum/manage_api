<?php

namespace App\Models\Store;

use App\Models\Model;

class Address extends Model
{
    
    protected $connection = 'store';
    
    protected $table = 'store_address';

    protected $primaryKey = 'id';

    protected $guarded = [];
}
