<?php
/*
 * @author: ChenGuangHui
 */
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/9/22 15:49
 */


namespace App\Models\Store;


use App\Models\Model;

class OrderGoods extends Model
{
    protected $connection = 'store';

    protected $table = 'store_order_goods';

    protected $primaryKey = 'id';
}

