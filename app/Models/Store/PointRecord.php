<?php

namespace App\Models\Store;

use App\Models\Model;

class PointRecord extends Model
{
    
    protected $connection = 'store';

    protected $table = 'store_point_record';

    protected $primaryKey = 'id';

    protected $guarded = [];
}
