<?php
/*
 * @author: ChenGuangHui
 */
/*
 * @author: ChenGuangHui
 */
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/7/8 10:09
 */


namespace App\Models\Store;


use App\Models\Model;

class Lexicon extends Model
{
    protected $connection = 'store';

    protected $table = 'store_lexicon';

    protected $primaryKey = 'id';

    /**
     * @return bool
     */
    public function getStatusAttribute()
    {
        return $this->attributes['status'] = $this->attributes['status'] ? true : false;
    }

    /**
     * @return mixed
     */
    public function setStatusAttribute()
    {
        return $this->attributes['status'] = $this->attributes['status'] ? 1 : 0;
    }
}
