<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2022/04/13 11:00
 */

namespace App\Models\Store;

use App\Models\Model;

class BlindBox extends Model
{
    
    protected $connection = 'store';

    protected $table = 'store_blind_box';

    protected $primaryKey = 'id';
    
    protected $guarded = [];
}
