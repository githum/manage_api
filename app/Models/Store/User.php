<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2022/04/13 11:00
 */

namespace App\Models\Store;

use App\Models\Model;

class User extends Model
{
    protected $connection = 'store';

    protected $table = 'store_user';

    protected $primaryKey = 'id';

    protected $guarded = [];

    protected $appends = ['status_name', 'sex_name', 'vip_name'];

    /**
     * @return bool
     */
    public function getStatusAttribute()
    {
        return $this->attributes['status'] = $this->attributes['status'] ? true : false;
    }

    
    /**
     * @author ChenGuangHui
     * @dateTime 2022-05-07
     */
    public function getStatusNameAttribute()
    {
        return $this->attributes['status'] == 1 ? '正常' : '禁用';
    }

    /**
     * @author ChenGuangHui
     * @dateTime 2022-05-07
     */
    public function getSexNameAttribute()
    {
        if ($this->attributes['sex'] == 0) {
            return '保密';
        } else {
            return $this->attributes['sex'] == 1 ? '男' : '女';
        }
    }

    
    /**
     * @author ChenGuangHui
     * @dateTime 2022-05-07
     */
    public function getVipNameAttribute()
    {
        return '普通会员';
    }

}
