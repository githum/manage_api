<?php
/*
 * @author: ChenGuangHui
 */
/*
 * @author: ChenGuangHui
 */

namespace App\Models\Store;

use App\Models\Model;


class Servers extends Model
{
    
    protected $connection = 'store';

    protected $table = 'store_servers';

    protected $primaryKey = 'id';

    protected $guarded = [];
}
