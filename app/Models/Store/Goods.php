<?php
/*
 * @author: ChenGuangHui
 */
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2022/04/13 11:00
 */

namespace App\Models\Store;

use App\Models\Model;

class Goods extends Model
{
    protected $connection = 'store';
    
    protected $table = 'store_goods';

    protected $primaryKey = 'goods_id';
    

    public function goodsSku()
    {
        return $this->hasMany(GoodsSku::class, 'goods_id', 'goods_id');
    }

    public function goodsSpec()
    {
        return $this->hasMany(GoodsSpec::class, 'goods_id', 'goods_id');
    }
}
