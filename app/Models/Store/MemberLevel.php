<?php
/*
 * @author: ChenGuangHui
 */

namespace App\Models\Store;

use App\Models\Model;

class MemberLevel extends Model
{
    
    protected $connection = 'store';
    
    protected $table = 'store_member_level';

    protected $primaryKey = 'id';

    protected $guarded = [];
}
