<?php
/*
 * @author: ChenGuangHui
 */
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2022/04/13 11:00
 */

namespace App\Models\Store;

use App\Models\Model;
use App\Models\Store\SpecValue;

class GoodsSpec extends Model
{
    protected $connection = 'store';
    
    protected $table = 'store_goods_spec';

    protected $primaryKey = 'id';

    public function SpecValue()
    {
        return $this->hasOne(SpecValue::class, 'id', 'spec_value_id');
    }
}
