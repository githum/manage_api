<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2022/04/13 11:00
 */

namespace App\Models\Store;

use App\Models\Model;

class Help extends Model
{
    
    protected $connection = 'store';

    protected $table = 'store_help';

    protected $primaryKey = 'id';
    
    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(Help::class, 'parent_id', 'id');
    }
}
