<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/9/22 15:49
 */


namespace App\Models\Store;


use App\Models\Model;
use App\Models\Store\User;

class Order extends Model
{
    protected $connection = 'store';
    
    protected $table = 'store_order';

    protected $primaryKey = 'order_id';

    protected $appends = ['status_name'];

    // 订单状态[1、未付款，2、已付款，3、未发货，4、已发货，5、交易成功，6、交易关闭]
    const WAIT_PAID_ORDER = 1;
    const SUCCESS_PAID_ORDER = 2;
    const WAIT_SHIPPED_ORDER = 3;
    const HAVE_SHIPPED_ORDER = 4;
    const TRADING_SUCCESS_ORDER = 5;
    const TRADING_CLOSED_ORDER = 6;

     // 订单状态
     public static $status = [
        self::WAIT_PAID_ORDER         =>      '未付款',
        self::SUCCESS_PAID_ORDER      =>      '已付款',
        self::WAIT_SHIPPED_ORDER      =>      '未发货',
        self::HAVE_SHIPPED_ORDER      =>      '已发货',
        self::TRADING_SUCCESS_ORDER   =>      '交易成功',
        self::TRADING_CLOSED_ORDER    =>      '交易关闭',
    ];

    /**
     * @author ChenGuangHui
     * @dateTime 2022-05-07
     */
    public function getStatusNameAttribute()
    {
        return isset($this->attributes['status']) ? self::$status[$this->attributes['status']] : '待处理';
    }

    public function orderGoods()
    {
        return $this->hasMany(OrderGoods::class, 'order_id', 'order_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'uid', 'uid');
    }
}

