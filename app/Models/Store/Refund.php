<?php
/*
 * @author: ChenGuangHui
 */
/*
 * @author: ChenGuangHui
 */
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/9/22 15:49
 */


namespace App\Models\Store;


use App\Models\Model;
use App\Models\Store\User;

class Refund extends Model
{
    protected $connection = 'store';
    
    protected $table = 'store_order_refund';

    protected $primaryKey = 'id';

    protected $appends = ['status_name'];

    // 订单状态[1、售后申请，2、售后处理中，3、售后完成]
    const WAIT_PAID_ORDER = 1;
    const SUCCESS_PAID_ORDER = 2;
    const WAIT_SHIPPED_ORDER = 3;

     // 订单状态
     public static $status = [
        self::WAIT_PAID_ORDER         =>      '未付款',
        self::SUCCESS_PAID_ORDER      =>      '已付款',
        self::WAIT_SHIPPED_ORDER      =>      '未发货',
    ];

    /**
     * @author ChenGuangHui
     * @dateTime 2022-05-07
     */
    public function getStatusNameAttribute()
    {
        return isset($this->attributes['status']) ? self::$status[$this->attributes['status']] : '待处理';
    }

    public function user()
    {
        return $this->hasOne(User::class, 'uid', 'uid');
    }
}

