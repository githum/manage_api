<?php
/*
 * @author: ChenGuangHui
 */

namespace App\Models\Store;

use App\Models\Model;

class MemberUseful extends Model
{
    
    protected $connection = 'store';

    protected $table = 'store_member_useful';

    protected $primaryKey = 'id';

    protected $guarded = [];
}
