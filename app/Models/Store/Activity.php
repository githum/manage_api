<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2022/04/13 11:00
 */

namespace App\Models\Store;

use App\Models\Model;

class Activity extends Model
{
    
    protected $connection = 'store';

    protected $table = 'store_activity';

    protected $primaryKey = 'id';
    
    protected $guarded = [];

    /**
     * @return bool
     */
    public function getStatusAttribute()
    {
        return $this->attributes['status'] = $this->attributes['status'] ? true : false;
    }

}
