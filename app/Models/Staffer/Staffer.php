<?php

namespace App\Models\Staffer;

use App\Models\Tenant;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Overtrue\EasySms\PhoneNumber;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\Staffer\Staffer
 *
 * @property int $id
 * @property string $name 姓名
 * @property string $email 邮箱地址
 * @property bool $status 账号状态[0、 正常;1、禁用]
 * @property string|null $role_ids 角色Ids
 * @property string $mobile 手机号码
 * @property int $tenant_id 租户Id
 * @property string $nickname 昵称
 * @property string|null $register_ip 注册ip
 * @property int|null $is_liable 是否责任人： 0、 否  1、是
 * @property string $password 账户密码
 * @property string $avatar_url 头像
 * @property string|null $remember_token
 * @property \datetime|null $created_at 创建时间
 * @property \datetime|null $updated_at 更新时间
 * @property string|null $deleted_at 删除时间
 * @property string|null $remark 备注
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read int|null $revision_history_count
 * @property-read Tenant|null $tenant
 * @method static \Illuminate\Database\Eloquent\Builder|Staffer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Staffer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Staffer query()
 * @method static \Illuminate\Database\Eloquent\Builder|Staffer whereAvatarUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staffer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staffer whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staffer whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staffer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staffer whereIsLiable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staffer whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staffer whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staffer whereNickname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staffer wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staffer whereRegisterIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staffer whereRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staffer whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staffer whereRoleIds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staffer whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staffer whereTenantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staffer whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Staffer extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable, RevisionableTrait;

    protected $revisionEnabled = true;

    protected $historyLimit = 500; //Maintain a maximum of 500 changes at any point of time, while cleaning up old revisions.

    // 业务标识
    const TIP_CODE = 'staffer';

    static $unguarded = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'entry_time' => 'datetime:Y-m-d',
        'leave_time' => 'datetime:Y-m-d',
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
        'email_verified_at' => 'datetime',
    ];

    /**
     * Route notifications for the mail channel.
     *
     * @return string
     */
    public function routeNotificationForMail()
    {
        return $this->email;
    }

    /**
     * Route notifications for the sms channel.
     *
     * @return string
     */
    public function routeNotificationForEasySms()
    {
        return new PhoneNumber($this->mobile, '86');
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
    
    public function tenant()
    {
        return $this->hasOne(Tenant::class, 'id', 'tenant_id');
    }

    /**
     * @return bool
     */
    public function getStatusAttribute()
    {
        return $this->attributes['status'] = $this->attributes['status'] ? true : false;
    }
}
