<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/8/5 14:03
 */


namespace App\Models\Material;


use App\Models\Model;

/**
 * App\Models\Material\MaterialDir
 *
 * @property int $id ID
 * @property string $name 文件夹名
 * @property int $sort 排序
 * @property int $type 类型： 1、私密 2、共享
 * @property int $created_id 创建人Id
 * @property int $parent_id 上级ID
 * @property \Illuminate\Support\Carbon $created_at 创建时间
 * @property \Illuminate\Support\Carbon|null $updated_at 更新时间
 * @property-read \Illuminate\Database\Eloquent\Collection|MaterialDir[] $children
 * @property-read int|null $children_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Material\Material[] $material
 * @property-read int|null $material_count
 * @method static \Illuminate\Database\Eloquent\Builder|MaterialDir newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MaterialDir newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MaterialDir query()
 * @method static \Illuminate\Database\Eloquent\Builder|MaterialDir whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MaterialDir whereCreatedId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MaterialDir whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MaterialDir whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MaterialDir whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MaterialDir whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MaterialDir whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MaterialDir whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class MaterialDir extends Model
{
    protected $table = 'material_dir';

    protected $primaryKey = 'id';

    protected $fillable = ['name', 'sort', 'parent_id', 'created_at', 'updated_at'];

    public function material()
    {
        return $this->hasMany(Material::class, 'dir_id', 'id');
    }

    public function children()
    {
        return $this->hasMany(MaterialDir::class, 'parent_id', 'id')->with('children');
    }
}
