<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/8/5 14:03
 */


namespace App\Models\Material;


use App\Models\Model;

/**
 * App\Models\Material\Material
 *
 * @property int $id ID
 * @property string $name 文件名
 * @property int $sort 排序
 * @property string|null $file_suffix 文件类型
 * @property string|null $size 文件大小
 * @property int $dir_id 文件夹ID
 * @property string $path 文件路径
 * @property string $hash 文件hash值
 * @property int $type 文件类型: 1 普通 2 文档
 * @property int $created_id 创建人
 * @property string $ext 文件名后缀
 * @property \Illuminate\Support\Carbon $created_at 创建时间
 * @property \Illuminate\Support\Carbon|null $updated_at 更新时间
 * @method static \Illuminate\Database\Eloquent\Builder|Material newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Material newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Material query()
 * @method static \Illuminate\Database\Eloquent\Builder|Material whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Material whereCreatedId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Material whereDirId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Material whereExt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Material whereFileSuffix($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Material whereHash($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Material whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Material whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Material wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Material whereSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Material whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Material whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Material whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Material extends Model
{
    protected $table = 'material';

    protected $primaryKey = 'id';

    // 业务标识
    const TIP_CODE = 'material';
}
