<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/8/1 11:00
 */

namespace App\Models\Document;

use App\Models\Model;

/**
 * App\Models\Document\Document
 *
 * @property int $id 笔记ID
 * @property int $staffer_id 用户ID
 * @property int $class_id 分类ID
 * @property string $title 笔记标题
 * @property string $abstract 笔记摘要
 * @property int $is_asterisk 是否星标笔记[0:否;1:是]
 * @property int $status 笔记状态[1:正常;2:已删除]
 * @property string $md_content Markdown 内容
 * @property string $content Markdown 解析HTML内容
 * @property string|null $deleted_at 笔记删除时间
 * @property \Illuminate\Support\Carbon|null $created_at 添加时间
 * @property \Illuminate\Support\Carbon|null $updated_at 最后一次更新时间
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Document\DocumentAnnex[] $files
 * @property-read int|null $files_count
 * @method static \Illuminate\Database\Eloquent\Builder|Document newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Document newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Document query()
 * @method static \Illuminate\Database\Eloquent\Builder|Document whereAbstract($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Document whereClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Document whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Document whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Document whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Document whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Document whereIsAsterisk($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Document whereMdContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Document whereStafferId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Document whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Document whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Document whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Document extends Model
{
    protected $table = 'document';

    protected $primaryKey = 'id';
    // 业务标识
    const TIP_CODE = 'document';

    /**
     * @return int
     */
    public function setIsAsteriskAttribute()
    {
        return $this->attributes['is_asterisk'] = $this->attributes['is_asterisk'] ? 1 : 0;
    }

    public function files()
    {
        return $this->hasMany(DocumentAnnex::class, 'doc_id', 'id');
    }

}
