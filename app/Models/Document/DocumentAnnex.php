<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/8/1 11:00
 */

namespace App\Models\Document;

use App\Models\Model;

/**
 * App\Models\Document\DocumentAnnex
 *
 * @property int $id 文件ID
 * @property int $staffer_id 上传文件的用户ID
 * @property int $doc_id 笔记ID
 * @property int $file_size 文件大小（单位字节）
 * @property string|null $path 文件保存地址（相对地址）
 * @property string|null $original_name 原文件名
 * @property string $hash 文件哈希
 * @property int $status 附件状态[1:正常;2:已删除]
 * @property \Illuminate\Support\Carbon $created_at 附件上传时间
 * @property \Illuminate\Support\Carbon|null $updated_at 更新时间
 * @property string|null $deleted_at 附件删除时间
 * @property string|null $file_suffix 附件类型
 * @method static \Illuminate\Database\Eloquent\Builder|DocumentAnnex newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DocumentAnnex newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DocumentAnnex query()
 * @method static \Illuminate\Database\Eloquent\Builder|DocumentAnnex whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocumentAnnex whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocumentAnnex whereDocId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocumentAnnex whereFileSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocumentAnnex whereFileSuffix($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocumentAnnex whereHash($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocumentAnnex whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocumentAnnex whereOriginalName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocumentAnnex wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocumentAnnex whereStafferId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocumentAnnex whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocumentAnnex whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class DocumentAnnex extends Model
{
    protected $table = 'document_annex';

    protected $primaryKey = 'id';
}
