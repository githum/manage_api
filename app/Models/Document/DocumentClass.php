<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/8/1 11:00
 */

namespace App\Models\Document;

use App\Models\Model;

/**
 * App\Models\Document\DocumentClass
 *
 * @property int $id 笔记分类ID
 * @property int $staffer_id 用户ID
 * @property string $class_name 分类名
 * @property int $sort 排序
 * @property int $is_default 默认分类[1:是;0:不是]
 * @property \Illuminate\Support\Carbon $created_at 创建时间
 * @property \Illuminate\Support\Carbon|null $updated_at 更新时间
 * @method static \Illuminate\Database\Eloquent\Builder|DocumentClass newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DocumentClass newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DocumentClass query()
 * @method static \Illuminate\Database\Eloquent\Builder|DocumentClass whereClassName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocumentClass whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocumentClass whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocumentClass whereIsDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocumentClass whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocumentClass whereStafferId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocumentClass whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class DocumentClass extends Model
{
    protected $table = 'document_class';

    protected $primaryKey = 'id';
}
