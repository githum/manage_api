<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/9/16 10:49
 */


namespace App\Models\Notice;


use App\Models\Model;

/**
 * App\Models\Notice\NoticeToStaffer
 *
 * @property int $id
 * @property int $staffer_id 员工ID
 * @property int $notice_id 消息ID
 * @property int $is_red 是否已读： 0 否 1是
 * @property string|null $updated_at 修改时间
 * @property string|null $created_at 添加时间
 * @method static \Illuminate\Database\Eloquent\Builder|NoticeToStaffer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|NoticeToStaffer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|NoticeToStaffer query()
 * @method static \Illuminate\Database\Eloquent\Builder|NoticeToStaffer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NoticeToStaffer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NoticeToStaffer whereIsRed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NoticeToStaffer whereNoticeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NoticeToStaffer whereStafferId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NoticeToStaffer whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class NoticeToStaffer extends Model
{
    protected $table = 'notice_to_staffer';

    protected $primaryKey = 'id';

    public $timestamps = false;
}
