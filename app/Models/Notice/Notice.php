<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/7/16 16:49
 */


namespace App\Models\Notice;


use App\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Notice\Notice
 *
 * @property int $id
 * @property string $title 标题
 * @property int $type 类型：0 、所有人, 1、群发
 * @property string $abstract 摘要
 * @property string|null $clump_ids 发布范围
 * @property int|null $class_id 分类ID
 * @property int $status 状态：0 、待发送, 1、已发送，2、撤回
 * @property string $md_content Markdown 内容
 * @property string $content Markdown 解析HTML内容
 * @property string $jump_url 跳转地址
 * @property int $created_id 创建人Id
 * @property string $created_name 创建人名称
 * @property \Illuminate\Support\Carbon $created_at 创建时间
 * @property \Illuminate\Support\Carbon|null $updated_at 更新时间
 * @property string|null $deleted_at 删除时间
 * @method static \Illuminate\Database\Eloquent\Builder|Notice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Notice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Notice query()
 * @method static \Illuminate\Database\Eloquent\Builder|Notice whereAbstract($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notice whereClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notice whereClumpIds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notice whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notice whereCreatedId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notice whereCreatedName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notice whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notice whereJumpUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notice whereMdContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notice whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notice whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notice whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notice whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Notice\NoticeToStaffer[] $staffer
 * @property-read int|null $staffer_count
 * @method static \Illuminate\Database\Query\Builder|Notice onlyTrashed()
 * @method static \Illuminate\Database\Query\Builder|Notice withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Notice withoutTrashed()
 */
class Notice extends Model
{
    use SoftDeletes;

    protected $table = 'notice';

    protected $primaryKey = 'id';

    // 业务标识
    const TIP_CODE = 'notice';

    public function staffer()
    {
        return $this->hasMany(NoticeToStaffer::class, 'notice_id', 'id');
    }
}
