<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/9/6 17:07
 */


namespace App\Models\Notice;


use App\Models\Model;

/**
 * App\Models\Notice\NoticeClass
 *
 * @property int $id 笔记分类ID
 * @property int $staffer_id 用户ID
 * @property string $class_name 分类名
 * @property int $is_default 默认分类[1:是;0:不是]
 * @property \Illuminate\Support\Carbon $created_at 创建时间
 * @property \Illuminate\Support\Carbon|null $updated_at 更新时间
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Notice\Notice[] $children
 * @property-read int|null $children_count
 * @method static \Illuminate\Database\Eloquent\Builder|NoticeClass newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|NoticeClass newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|NoticeClass query()
 * @method static \Illuminate\Database\Eloquent\Builder|NoticeClass whereClassName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NoticeClass whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NoticeClass whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NoticeClass whereIsDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NoticeClass whereStafferId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NoticeClass whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class NoticeClass extends Model
{
    protected $table = 'notice_class';

    protected $primaryKey = 'id';

    public function children()
    {
        return $this->hasMany(Notice::class, 'class_id', 'id');
    }
}
