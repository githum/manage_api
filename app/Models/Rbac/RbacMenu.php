<?php


namespace App\Models\Rbac;

use App\Models\Model;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\Rbac\RbacMenu
 *
 * @property int $id
 * @property string $title 权限名称
 * @property string $name 权限别名
 * @property string $icon 图标
 * @property int $sort 排序
 * @property string $component 模块
 * @property bool $status 是否禁用: 0 禁用 1正常
 * @property string|null $auth_code 权限标识
 * @property string $path 路由地址
 * @property bool $is_affix 是否内嵌: 0 否 1 是
 * @property int|null $is_dir 是否目录：0 否 1是
 * @property int $parent_id 上级ID
 * @property string|null $menu_ids 父级id列表
 * @property string $is_link 跳转链接
 * @property bool $is_hide 是否禁用: 0 禁用 1正常
 * @property bool $is_iframe 是否内嵌: 0 否 1 是
 * @property string $description 菜单描述
 * @property \Illuminate\Support\Carbon|null $created_at 创建时间
 * @property \Illuminate\Support\Carbon|null $updated_at 更新时间
 * @property-read \Illuminate\Database\Eloquent\Collection|RbacMenu[] $children
 * @property-read int|null $children_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Rbac\RbacAction[] $meta
 * @property-read int|null $meta_count
 * @property-read RbacMenu|null $parent
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read int|null $revision_history_count
 * @method static \Illuminate\Database\Eloquent\Builder|RbacMenu newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RbacMenu newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RbacMenu query()
 * @method static \Illuminate\Database\Eloquent\Builder|RbacMenu whereAuthCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RbacMenu whereComponent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RbacMenu whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RbacMenu whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RbacMenu whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RbacMenu whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RbacMenu whereIsAffix($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RbacMenu whereIsDir($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RbacMenu whereIsHide($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RbacMenu whereIsIframe($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RbacMenu whereIsLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RbacMenu whereMenuIds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RbacMenu whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RbacMenu whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RbacMenu wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RbacMenu whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RbacMenu whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RbacMenu whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RbacMenu whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class RbacMenu extends Model
{
    use RevisionableTrait;

    protected $revisionEnabled = true;

    protected $table = 'rbac_menu';

    protected $primaryKey = 'id';

    protected $guarded = [];

    // 业务标识
    const TIP_CODE = 'rbac_menu';

    /**
     * @return bool
     */
    public function getStatusAttribute()
    {
        return $this->attributes['status'] = $this->attributes['status'] ? true : false;
    }

    /**
     * @return bool
     */
    public function getIsHideAttribute()
    {
        return $this->attributes['is_hide'] = $this->attributes['is_hide'] ? true : false;
    }

    /**
     * @return bool
     */
    public function getIsAffixAttribute()
    {
        return $this->attributes['is_affix'] = $this->attributes['is_affix'] ? true : false;
    }

    /**
     * @return bool
     */
    public function getIsIframeAttribute()
    {
        return $this->attributes['is_iframe'] = $this->attributes['is_iframe'] ? true : false;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(RbacMenu::class, 'parent_id', 'id')->with('children', 'meta')->where('status', 0)->orderByDesc('sort');
    }

    public function parent()
    {
        return $this->hasOne(RbacMenu::class, 'id', 'parent_id')->with('parent')->orderByDesc('sort');
    }
}

