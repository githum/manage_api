<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/7/15 17:25
 */


namespace App\Models\Rbac;


use App\Models\Model;
use App\Models\Tenant;

/**
 * App\Models\Rbac\RbacRole
 *
 * @property int $id
 * @property string $name 角色名称
 * @property int $status 状态[0、 正常;1、禁用]
 * @property int $tenant_id 租户id
 * @property string $menus 角色权限
 * @property string $remark 备注
 * @property \Illuminate\Support\Carbon $created_at 创建时间
 * @property \Illuminate\Support\Carbon|null $updated_at 更新时间
 * @property-write mixed $updated_id
 * @property-write mixed $updated_name
 * @property-read Tenant|null $tenant
 * @method static \Illuminate\Database\Eloquent\Builder|RbacRole newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RbacRole newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RbacRole query()
 * @method static \Illuminate\Database\Eloquent\Builder|RbacRole whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RbacRole whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RbacRole whereMenus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RbacRole whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RbacRole whereRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RbacRole whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RbacRole whereTenantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RbacRole whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class RbacRole extends Model
{
    protected $table = 'rbac_role';

    protected $primaryKey = 'id';

    /**
     * @return int|string|null
     */
    public function setUpdatedIdAttribute()
    {
        return $this->attributes['updated_id'] = auth('api')->id();
    }

    /**
     * @return mixed
     */
    public function setUpdatedNameAttribute()
    {
        return $this->attributes['updated_name'] = auth('api')->user()->name;
    }

    public function tenant()
    {
        return $this->hasOne(Tenant::class, 'id', 'tenant_id');
    }
}
