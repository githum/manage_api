<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2022/04/13 11:00
 */

namespace App\Models;

use App\Models\Model;

/**
 * App\Models\Tenant
 *
 * @property int $id 主键ID
 * @property string $name 租户名称
 * @property bool $status 租户状态[1-正常2-停用]
 * @property string $domain 唯一码
 * @property string $logo 租户Logo地址
 * @property string|null $valid_start_at 有效开始时间
 * @property string|null $valid_end_at 有效结束时间
 * @property string $host 数据库host
 * @property string $database database
 * @property string $port 端口号
 * @property string $account 数据库账号
 * @property string $password 数据库密码
 * @property string $wx_appid 微信appId
 * @property string $wx_secret 微信secret
 * @property string $login_background 登录页背景图地址
 * @property string $copyright 租户版权
 * @property string|null $remark 备注
 * @property \Illuminate\Support\Carbon|null $created_at 创建时间
 * @property \Illuminate\Support\Carbon|null $updated_at 更新时间
 * @method static \Illuminate\Database\Eloquent\Builder|Tenant newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Tenant newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Tenant query()
 * @method static \Illuminate\Database\Eloquent\Builder|Tenant whereAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tenant whereCopyright($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tenant whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tenant whereDatabase($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tenant whereDomain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tenant whereHost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tenant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tenant whereLoginBackground($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tenant whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tenant whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tenant wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tenant wherePort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tenant whereRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tenant whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tenant whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tenant whereValidEndAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tenant whereValidStartAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tenant whereWxAppid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tenant whereWxSecret($value)
 * @mixin \Eloquent
 */
class Tenant extends Model
{
    protected $table = 'tenant';

    protected $primaryKey = 'id';
    
    protected $guarded = [];

    /**
     * @return bool
     */
    public function getStatusAttribute()
    {
        return $this->attributes['status'] = $this->attributes['status'] ? true : false;
    }

}
