<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/9/6 16:05
 */


namespace App\Models\Common;


use App\Models\Model;

/**
 * App\Models\Common\Feedback
 *
 * @property int $id
 * @property int $staffer_id 用户ID
 * @property int $type 类型: 1 代码错误 2 安全相关 3 性能问题 4 设计缺陷 5 其他
 * @property string $title 标题
 * @property int $status 状态： 1 待处理 2 已处理
 * @property string|null $annex 异常附件
 * @property string $md_content Markdown 内容
 * @property string $content Markdown 解析HTML内容
 * @property \Illuminate\Support\Carbon $created_at 创建时间
 * @property \Illuminate\Support\Carbon|null $updated_at 更新时间
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback query()
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereAnnex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereMdContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereStafferId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Feedback extends Model
{
    protected $table = 'feedback';

    protected $primaryKey = 'id';
}
