<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/5/13 16:52
 */

namespace App\Logic\Order;


use App\Enums\HttpCode;
use App\Logic\CommLogic;
use App\Models\Store\GoodsSpec;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Store\Order;
use Arr;

class OrderLogic extends CommLogic
{

    /**
     * @param array $where
     * @param array $pageData
     * @param array $sortData
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($where = [], $pageData = ['pageSize' => 20, 'pageNow' => 1], $sortData = [])
    {
        $query = Order::query()
            ->with(['orderGoods', 'user'])
            ->when(isset($where['uid']), function(Builder $query) use ($where){
                $query->where('uid', $where['uid']);
            })
            ->when(isset($where['status']), function (Builder $query) use ($where) {
                if ($where['status'] == 2) {
                    $query->where('status', Order::WAIT_PAID_ORDER);
                } elseif ($where['status'] == 3) {
                    $query->whereIn('status', [Order::SUCCESS_PAID_ORDER, Order::WAIT_SHIPPED_ORDER]);
                } elseif ($where['status'] == 4) {
                    $query->where('status', Order::HAVE_SHIPPED_ORDER);
                } elseif ($where['status'] == 5) {
                    $query->where('status', Order::TRADING_SUCCESS_ORDER);
                } else {
                    # 退款、售后
                    # code...
                }
            })
            ->when(!empty($where['name']), function (Builder $query) use ($where) {
                $query->where('name', 'like', '%' . $where['name'] . '%');
            });

        if (!empty($sortData)) {
            $query = $query->orderBy($sortData['sortRow'], $sortData['sortType']);
        }else{
            $query = $query->orderBy('order_id', 'desc');
        }

        $data = $query->paginate($pageData['pageSize'], ['*'], 'page', $pageData['pageNow'])
            ->toArray();
        return $data;
    }

    /**
     * @param $params
     * @param $orderId
     * @return \App\Models\Model|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function show($params, $orderId)
    {
        $order = Order::query()->with('orderGoods')->where('order_id', $orderId)->first();
        abort_if(empty($order), HttpCode::FORBIDDEN, '订单不存在！');
        $data = $order->toArray();
        $ids = explode(',', implode(',', array_column($data["order_goods"], 'sku')));
        $goodsSpec = GoodsSpec::query()->with('SpecValue')->whereIn('id', array_unique($ids))->get()->toArray();
        $spec_value = array_column($goodsSpec, 'spec_value', 'id');
        $data['gift_growth'] = $data['gift_point'] = $data['deductt_point'] = $data['coupon_discount'] = 0;
        foreach($data['order_goods'] as &$item){
            // $item['goods_thumb'] = Arr::first(array_column(json_decode($item['goods_thumb'], true), 'url'));
            $sku_name = '';
            if (!empty($item["sku"])) {
                foreach(explode(",", $item["sku"]) as $id){
                    if (isset($spec_value[$id])) {
                        $sku_name .= $spec_value[$id]['spec_value'] . ';';
                    }
                }
            }

            $data['gift_growth'] += $item['gift_growth'];
            $data['gift_point'] += $item['gift_point'];
            $data['deductt_point'] += $item['deductt_point'];
            $data['coupon_discount'] += $item['coupon_discount'];
            $item['sku_name'] = rtrim($sku_name, ';') ?? '-';
        }
        return $data;
    }

    /**
     * @param $params
     * @param $orderId
     * @return bool|int
     */
    public function update($params, $orderId)
    {
        $lockName = 'order_update_' . auth('api')->id();
        $lock = \Cache::lock($lockName, 5);
        abort_if(!$lock->get(), HttpCode::TOO_MANY_REQUESTS, '请勿重复操作！');

        $data = Order::query()->where('order_id', $orderId)->first();
        abort_if(empty($data), HttpCode::FORBIDDEN, '订单不存在！');
        return Order::query()->where('order_id', $orderId)->update($params);
    }

    /**
     * @param $orderId
     * @return bool|mixed|null
     */
    public function destroy($orderId)
    {
        $data = Order::query()->where('order_id', $orderId)->first();
        abort_if(empty($data), HttpCode::FORBIDDEN, '订单不存在！');
        return Order::query()->where('order_id', $orderId)->delete();
    }

    /**
     * 物流发货
     * 
     * @param $orderId
     * @return bool|mixed|null
     */
    public function ship($orderId)
    {
        $order = Order::query()->where('order_id', $orderId)->first();
        abort_if(empty($order), HttpCode::FORBIDDEN, '订单不存在！');
        $data = array(
            'contact' => $order->contact,
            'moblie' => $order->moblie,
            'shipping_name' => $order->shipping_name,
            'shipping_code' => $order->shipping_code,
            'address' => $order->address,
        );

        return Order::query()->where('order_id', $orderId)->update($data);
    }

    /**
     * @author ChenGuangHui
     * @dateTime 2022-05-17
     * @param  [type] $params
     */
    public function analyze($params)
    {
        list($beginTime, $endTime) = $params['time'];
        $beginTimeDate = $beginTime;
        $xValue = [date('Y-m-d', strtotime($beginTime))];
        if($endTime > $beginTime){
            do {
                $beginTime = date('Y-m-d', strtotime('+1 days', strtotime($beginTime)));
                $xValue[] = $beginTime;
            } while (strtotime($beginTime) < strtotime($endTime));
        }
        $endDataTime = date('Y-m-d 23:59:59', strtotime($endTime));
        $startDataTime = date('Y-m-d 00:00:00', strtotime($beginTimeDate));

        // 汇总行
        $fields = [
            self::raw("FROM_UNIXTIME(UNIX_TIMESTAMP(created_at),'%Y-%m-%d') as datadate"),
            self::raw('COALESCE(SUM(amount), 0) AS amount'),
        ];

        $order = Order::query()
            ->select($fields)
            ->whereBetween('created_at', [$startDataTime, $endDataTime])
            ->groupBy(\DB::raw("FROM_UNIXTIME(UNIX_TIMESTAMP(created_at),'%Y-%m-%d')"))->get()->keyBy('datadate')->toArray();

        $amount = [];
        foreach($xValue as $date){
            $amount[] = isset($order[$date])? $order[$date]["amount"] : 0;
        }

        $data = array(
            'datadate' => $xValue,
            'order_amount' => $amount
        );

        return $data;
    }
}
