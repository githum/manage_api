<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/5/13 16:52
 */

namespace App\Logic\Doc;


use App\Enums\HttpCode;
use App\Logic\CommLogic;
use App\Models\Document\Document;

class DocLogic extends CommLogic
{
    /**
     * @param $params
     * @return array
     */
    public function lists($params)
    {
        $list = Document::query()
            ->where('staffer_id', auth('api')->id())
            ->orderByDesc('created_at')
            ->get()
            ->toArray();

        $asterisk = collect($list)->where('is_asterisk', 1)->where('status', 1)->all();
        $recent = collect($list)->where('updated_at', '>', date('Y-m-d H:i:s', strtotime('-7 days')))->where('status', 1)->all();
        $recycle = collect($list)->where('status', 2)->all();
        return array(
            'recent' => array_values($recent),
            'asterisk' => array_values($asterisk),
            'recycle' => array_values($recycle),
        );
    }

    /**
     * @param $params
     * @return \App\Models\Model|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function store($params)
    {
        $lockName = 'document_store_' . auth('api')->id();
        $lock = \Cache::lock($lockName, 5);
        abort_if(!$lock->get(), HttpCode::TOO_MANY_REQUESTS, '请勿重复操作！');
        $data = array(
            'staffer_id' => auth('api')->id(),
            'title' => $params['title'],
            'class_id' => $params['class_id'],
            'abstract' => substr($params['content'], 0, 250),
            'content' => $params['content'],
            'md_content' => $params['md_content']
        );
        return Document::query()->create($data);
    }

    /**
     * @param $id
     * @return \App\Models\Model|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function show($id)
    {
        $data = Document::query()->with('files')->where('id', $id)->first();
        abort_if(empty($data), HttpCode::FORBIDDEN, '文档不存在！');
        return $data;
    }

    /**
     * @param $params
     * @param $id
     * @return bool|int
     */
    public function update($params, $id)
    {
        $lockName = 'document_update_' . auth('api')->id();
        $lock = \Cache::lock($lockName, 5);
        abort_if(!$lock->get(), HttpCode::TOO_MANY_REQUESTS, '请勿重复操作！');

        $data = Document::query()->where('id', $id)->first();
        abort_if(empty($data), HttpCode::FORBIDDEN, '文档不存在！');
        return Document::query()->where('id', $id)->update($params);
    }

    /**
     * @param $id
     * @return bool|int
     */
    public function asterisk($id)
    {
        $lockName = 'document_asterisk_' . auth('api')->id();
        $lock = \Cache::lock($lockName, 5);
        abort_if(!$lock->get(), HttpCode::TOO_MANY_REQUESTS, '请勿重复操作！');
        $doc = Document::query()->where('id', $id)->first();
        abort_if(empty($doc), HttpCode::FORBIDDEN, '文档不存在！');
        return Document::query()->where('id', $id)->update(array('is_asterisk' => !$doc->is_asterisk));
    }

    /**
     * @param $id
     * @return bool|mixed|null
     */
    public function destroy($id)
    {
        $data = Document::query()->where('id', $id)->first();
        abort_if(empty($data), HttpCode::FORBIDDEN, '文档不存在！');
        return Document::query()->where('id', $id)->delete();
    }
}
