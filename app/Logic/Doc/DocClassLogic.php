<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/5/13 16:52
 */

namespace App\Logic\Doc;


use App\Enums\HttpCode;
use App\Logic\CommLogic;
use App\Models\Document\Document;
use App\Models\Document\DocumentClass;

class DocClassLogic extends CommLogic
{
    /**
     * @param $params
     * @return array
     */
    public function lists($params)
    {
        $list = DocumentClass::query()
            ->orderByDesc('created_at')
            ->get()->toArray();

        $document = Document::query()->where('staffer_id', auth('api')->id())->where('status', 1)->get();
        foreach ($list as &$item) {
            $item['count'] = collect($document)->where('class_id', $item['id'])->count();
            $item['children'] = array_values(collect($document)->where('class_id', $item['id'])->all());
        }

        return $list;
    }

    /**
     * @param $params
     * @return \App\Models\Model|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function store($params)
    {
        $lockName = 'document_store_' . auth('api')->id();
        $lock = \Cache::lock($lockName, 5);
        abort_if(!$lock->get(), HttpCode::TOO_MANY_REQUESTS, '请勿重复操作！');
        $detailData = array(
            'staffer_id' => auth('api')->id(),
            'class_name' => $params['class_name'] ?? '',
        );
        return DocumentClass::query()->create($detailData);
    }

    /**
     * @param $params
     * @param $id
     * @return bool|int
     */
    public function update($params, $id)
    {
        $lockName = 'document_update_' . auth('api')->id();
        $lock = \Cache::lock($lockName, 5);
        abort_if(!$lock->get(), HttpCode::TOO_MANY_REQUESTS, '请勿重复操作！');
        $class = DocumentClass::query()->where('id', $id)->first();
        abort_if(empty($class), HttpCode::FORBIDDEN, '分类不存在！');
        if (isset($params['doc_id'])) {
            Document::query()->where('id', $params['doc_id'])->update(array('class_id' => $id));
        } else {
            DocumentClass::query()->where('id', $id)->update($params);
        }
        return true;
    }

    /**
     * @param $id
     * @return bool|mixed|null
     */
    public function destroy($id)
    {
        $data = DocumentClass::query()->where('id', $id)->first();
        abort_if(empty($data), HttpCode::FORBIDDEN, '分类不存在！');
        Document::query()->where('class_id', $id)->update(['status' => 1]);
        return DocumentClass::query()->where('id', $id)->delete();
    }
}
