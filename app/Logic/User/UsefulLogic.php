<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/4/30 11:35
 */


namespace App\Logic\User;


use App\Enums\HttpCode;
use App\Logic\CommLogic;
use App\Models\Store\MemberUseful;
use Arr;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\QueryException;

class UsefulLogic extends CommLogic
{
    /**
     * @param $params
     * @return mixed
     */
    public function lists($params)
    {
        $query = MemberUseful::query()
            ->when(isset($params['status']), function (Builder $query) use ($params) {
                $query->where('status', $params['status']);
            })
            ->when(!empty($params['name']), function (Builder $query) use ($params) {
                $query->where('name', 'like', '%' . $params['name'] . '%');
            });

        if (!empty($sortData)) {
            $query = $query->orderBy($sortData['sortRow'], $sortData['sortType']);
        }else{
            $query = $query->orderBy('id', 'desc');
        }

        $data = $query->get()->toArray();

        return $data;
    }

    /**
     * @param array $where
     * @param array $pageData
     * @param array $sortData
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($where = [], $pageData = ['pageSize' => 20, 'pageNow' => 1], $sortData = [])
    {
        $query = MemberUseful::query()
            ->when(isset($where['status']), function (Builder $query) use ($where) {
                $query->where('status', $where['status']);
            })
            ->when(!empty($where['name']), function (Builder $query) use ($where) {
                $query->where('name', '%' . $where['name'] . '%');
            });

        if (!empty($sortData)) {
            $query = $query->orderBy($sortData['sortRow'], $sortData['sortType']);
        }else{
            $query = $query->orderBy('id', 'desc');
        }

        $data = $query->paginate($pageData['pageSize'], ['*'], 'page', $pageData['pageNow'])
            ->toArray();

        foreach($data["data"] as &$item){
            $item['icon'] = Arr::first(array_column(json_decode($item['icon'], true), 'url'));
        }

        return $data;
    }

    /**
     * @param $params
     * @return bool
     * @throws \Throwable
     */
    public function store($params)
    {
        $lockName = 'useful_store_' . auth('api')->id();
        $lock = \Cache::lock($lockName, 5);
        abort_if(!$lock->get(), HttpCode::TOO_MANY_REQUESTS, '请勿重复操作！');
        \DB::beginTransaction();
        try {
            $data = array(
                'name' => $params['name'],
                'icon' => json_encode($params['icon'], true),
                'status' => $params['status'] ? 1 : 0,
                'remark' => $params['remark'] ?? '',
            );
            MemberUseful::query()->create($data);
            \DB::commit();
        } catch (QueryException $exception) {
            \DB::rollBack();
            throw new \Exception($exception->getMessage(), $exception->getCode());
        }
        return true;
    }

    /**
     * @param $params
     * @param $id
     * @return MemberUseful|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function show($params, $id)
    {
        $useful = MemberUseful::query()->where('id', $id)->first();
        abort_if(empty($useful), HttpCode::FORBIDDEN, '找不到会员');
        $useful->status = $useful->status == 1 ? true : false;
        $useful->icon = json_decode($useful->icon, true);
        return $useful;
    }

    /**
     * @param $params
     * @param $id
     * @return bool
     * @throws \Throwable
     */
    public function update($params, $id)
    {
        $lockName = 'useful_update_' . auth('api')->id();
        $lock = \Cache::lock($lockName, 5);
        abort_if(!$lock->get(), HttpCode::TOO_MANY_REQUESTS, '请勿重复操作！');

        $useful = MemberUseful::query()->where('id', $id)->first();
        abort_if(empty($useful), HttpCode::FORBIDDEN, '找不到会员');
        \DB::beginTransaction();
        try {
            $data = array(
                'name' => $params['name'],
                'icon' => json_encode($params['icon'], true),
                'status' => $params['status'] ? 1 : 0,
                'remark' => $params['remark'] ?? '',
            );
            $useful->update($data);
            \DB::commit();
        } catch (QueryException $exception) {
            \DB::rollBack();
            throw new \Exception($exception->getMessage(), $exception->getCode());
        }
        return true;
    }

    /**
     * @author ChenGuangHui
     * @dateTime 2022-05-17
     * @param  [type] $id
     */
    public function destroy($id)
    {
        $data = MemberUseful::query()->where('id', $id)->first();
        abort_if(empty($data), HttpCode::FORBIDDEN, '找不到会员');
        return $data->delete() ? true : false;
    }
}
