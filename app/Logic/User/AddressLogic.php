<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/4/30 11:35
 */


namespace App\Logic\User;


use App\Enums\HttpCode;
use App\Logic\CommLogic;
use App\Models\Staffer\Staffer;
use App\Models\Store\Address;
use Arr;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Hash;

class AddressLogic extends CommLogic
{
    /**
     * @param $params
     * @return mixed
     */
    public function lists($params)
    {
        $query = Address::query()
            ->when(isset($where['uid']), function(Builder $query) use ($params){
                $query->where('uid', $params['uid']);
            })
            ->when(isset($params['status']), function (Builder $query) use ($params) {
                $query->where('status', $params['status']);
            })
            ->when(!empty($params['name']), function (Builder $query) use ($params) {
                $query->where('name', 'like', '%' . $params['name'] . '%');
            });

        if (!empty($sortData)) {
            $query = $query->orderBy($sortData['sortRow'], $sortData['sortType']);
        }else{
            $query = $query->orderBy('id', 'desc');
        }

        $data = $query->get()->toArray();

        return $data;
    }

    /**
     * @param array $where
     * @param array $pageData
     * @param array $sortData
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($where = [], $pageData = ['pageSize' => 20, 'pageNow' => 1], $sortData = [])
    {
        $query = Address::query()
            ->where('uid', $where['uid'])
            ->when(isset($where['status']), function (Builder $query) use ($where) {
                $query->where('status', $where['status']);
            })
            ->when(!empty($where['name']), function (Builder $query) use ($where) {
                $query->where('name', '%' . $where['name'] . '%');
            });

        if (!empty($sortData)) {
            $query = $query->orderBy($sortData['sortRow'], $sortData['sortType']);
        }else{
            $query = $query->orderBy('id', 'desc');
        }

        $data = $query->paginate($pageData['pageSize'], ['*'], 'page', $pageData['pageNow'])
            ->toArray();

        return $data;
    }

    /**
     * @param $params
     * @param $id
     * @return Address|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function show($params, $id)
    {
        $address = Address::query()->where('id', $id)->first();
        abort_if(empty($address), HttpCode::FORBIDDEN, '找不到用户地址');
        $address->status = $address->status == 1 ? true : false;
        return $address;
    }

    /**
     * @param $params
     * @param $id
     * @return bool
     * @throws \Throwable
     */
    public function update($params, $id)
    {

        $lockName = 'address_update_' . auth('api')->id();
        $lock = \Cache::lock($lockName, 5);
        abort_if(!$lock->get(), HttpCode::TOO_MANY_REQUESTS, '请勿重复操作！');

        $address = Address::query()->where('id', $id)->first();
        abort_if(empty($address), HttpCode::FORBIDDEN, '找不到用户地址');
        \DB::beginTransaction();
        try {
            $data = array(
                'uid' => $params['uid'],
                'province' => $params['province'],
                'city' => $params['city'],
                'region' => $params['region'],
                'contact' => $params['contact'],
                'moblie' => $params['moblie'],
                'address' => $params['address'],
                'is_default' => $params['is_default'] ? 1 : 0,
            );
            $address->update($data);
            \DB::commit();
        } catch (QueryException $exception) {
            \DB::rollBack();
            throw new \Exception($exception->getMessage(), $exception->getCode());
        }
        return true;
    }

    /**
     * @author ChenGuangHui
     * @dateTime 2022-05-17
     * @param  [type] $id
     */
    public function destroy($id)
    {
        $data = Address::query()->where('id', $id)->first();
        abort_if(empty($data), HttpCode::FORBIDDEN, '找不到用户地址');
        return $data->delete() ? true : false;
    }
}
