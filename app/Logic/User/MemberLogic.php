<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/4/30 11:35
 */


namespace App\Logic\User;


use App\Enums\HttpCode;
use App\Logic\CommLogic;
use App\Models\Staffer\Staffer;
use App\Models\Store\MemberLevel;
use Arr;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Hash;

class MemberLogic extends CommLogic
{
    /**
     * @param $params
     * @return mixed
     */
    public function lists($params)
    {
        $query = MemberLevel::query()
            ->when(isset($params['status']), function (Builder $query) use ($params) {
                $query->where('status', $params['status']);
            })
            ->when(!empty($params['name']), function (Builder $query) use ($params) {
                $query->where('name', 'like', '%' . $params['name'] . '%');
            });

        if (!empty($sortData)) {
            $query = $query->orderBy($sortData['sortRow'], $sortData['sortType']);
        }else{
            $query = $query->orderBy('id', 'desc');
        }

        $data = $query->get()->toArray();

        return $data;
    }

    /**
     * @param array $where
     * @param array $pageData
     * @param array $sortData
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($where = [], $pageData = ['pageSize' => 20, 'pageNow' => 1], $sortData = [])
    {
        $query = MemberLevel::query()
            ->when(isset($where['status']), function (Builder $query) use ($where) {
                $query->where('status', $where['status']);
            })
            ->when(!empty($where['name']), function (Builder $query) use ($where) {
                $query->where('name', '%' . $where['name'] . '%');
            });

        if (!empty($sortData)) {
            $query = $query->orderBy($sortData['sortRow'], $sortData['sortType']);
        }else{
            $query = $query->orderBy('id', 'desc');
        }

        $data = $query->paginate($pageData['pageSize'], ['*'], 'page', $pageData['pageNow'])
            ->toArray();

        return $data;
    }

    /**
     * @param $params
     * @return bool
     * @throws \Throwable
     */
    public function store($params)
    {
        $lockName = 'level_store_' . auth('api')->id();
        $lock = \Cache::lock($lockName, 5);
        abort_if(!$lock->get(), HttpCode::TOO_MANY_REQUESTS, '请勿重复操作！');
        $data = array(
            'name' => $params['name'],
            'reach_growth' => $params['reach_growth'],
            'useful_ids' => implode(',', $params['useful_ids']),
            'status' => $params['status'] ? 1 : 0,
            'remark' => $params['remark'] ?? '',
        );
        MemberLevel::query()->create($data);
        return true;
    }

    /**
     * @param $params
     * @param $id
     * @return MemberLevel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function show($params, $id)
    {
        $level = MemberLevel::query()->where('id', $id)->first();
        abort_if(empty($level), HttpCode::FORBIDDEN, '找不到会员等级');
        $level->status = $level->status == 1 ? true : false;
        $useful_ids = [];
        foreach(explode(',', $level->useful_ids) as $id){
            $useful_ids[] = (int) $id;
        }
        $level->useful_ids = $useful_ids;
        return $level;
    }

    /**
     * @param $params
     * @param $id
     * @return bool
     * @throws \Throwable
     */
    public function update($params, $id)
    {

        $lockName = 'level_update_' . auth('api')->id();
        $lock = \Cache::lock($lockName, 5);
        abort_if(!$lock->get(), HttpCode::TOO_MANY_REQUESTS, '请勿重复操作！');

        $level = MemberLevel::query()->where('id', $id)->first();
        abort_if(empty($level), HttpCode::FORBIDDEN, '找不到会员等级');
        \DB::beginTransaction();
        try {
            $data = array(
                'name' => $params['name'],
                'reach_growth' => $params['reach_growth'],
                'useful_ids' => implode(',', $params['useful_ids']),
                'status' => $params['status'] ? 1 : 0,
                'remark' => $params['remark'] ?? '',
            );
            $level->update($data);
            \DB::commit();
        } catch (QueryException $exception) {
            \DB::rollBack();
            throw new \Exception($exception->getMessage(), $exception->getCode());
        }
        return true;
    }

    /**
     * @author ChenGuangHui
     * @dateTime 2022-05-17
     * @param  [type] $id
     */
    public function destroy($id)
    {
        $data = MemberLevel::query()->where('id', $id)->first();
        abort_if(empty($data), HttpCode::FORBIDDEN, '找不到会员等级');
        return $data->delete() ? true : false;
    }
}
