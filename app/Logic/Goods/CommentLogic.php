<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/5/23 16:52
 */

namespace App\Logic\Goods;

use App\Enums\HttpCode;
use App\Logic\CommLogic;
use App\Models\Store\Comment;
use App\Models\Store\Goods;
use Arr;
use Illuminate\Database\Eloquent\Builder;

class CommentLogic extends CommLogic
{
    /**
     * @param $params
     * @return mixed
     */
    public function lists($params)
    {
        $query = Comment::query()
            ->when(isset($params['status']), function (Builder $query) use ($params) {
                $query->where('status', $params['status']);
            })
            ->when(!empty($params['name']), function (Builder $query) use ($params) {
                $query->where('name', 'like', '%' . $params['name'] . '%');
            });

        if (!empty($sortData)) {
            $query = $query->orderBy($sortData['sortRow'], $sortData['sortType']);
        }else{
            $query = $query->orderBy('id', 'desc');
        }

        $data = $query->get()->toArray();
        return $data;
    }

    /**
     * @param array $where
     * @param array $pageData
     * @param array $sortData
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($where = [], $pageData = ['pageSize' => 20, 'pageNow' => 1], $sortData = [])
    {
        $query = Comment::query()
            ->when(isset($where['status']), function (Builder $query) use ($where) {
                $query->where('status', $where['status']);
            })
            ->when(!empty($where['name']), function (Builder $query) use ($where) {
                $query->where('name', 'like', '%' . $where['name'] . '%');
            });

        if (!empty($sortData)) {
            $query = $query->orderBy($sortData['sortRow'], $sortData['sortType']);
        }else{
            $query = $query->orderBy('id', 'desc');
        }
        $data = $query->paginate($pageData['pageSize'], ['*'], 'page', $pageData['pageNow'])->toArray();

        $category = Comment::query()->whereIn('id', array_column($data['data'], 'parent_id'))->get()->keyBy("id")->toArray();
        foreach($data['data'] as &$item){
            $item['thumb'] = Arr::first(array_column(json_decode($item['thumb'], true), 'url'));
            $item['parent_name'] = $item["parent_id"] > 0 ? $category[$item["parent_id"]]["name"] : '顶级分类';
        }

        return $data;
    }

    /**
     * @param $params
     * @return \App\Models\Model|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function store($params)
    {
        $lockName = 'category_store_' . auth('api')->id();
        $lock = \Cache::lock($lockName, 5);
        abort_if(!$lock->get(), HttpCode::TOO_MANY_REQUESTS, '请勿重复操作！');
        $data = array(
            'parent_id' => $params['parent_id'] ?? 0,
            'name' => $params['name'],
            'status' => $params['status'] ?? 1,
            'thumb' => json_encode($params['thumb'], true),
        );
        return Comment::query()->create($data);
    }

    /**
     * @param $categoryId
     * @return \App\Models\Model|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function show($data, $categoryId)
    {
        $data = Comment::query()->where('id', $categoryId)->first();
        abort_if(empty($data), HttpCode::FORBIDDEN, '分类不存在！');
        $categoryInfo = $data->toArray();
        $categoryInfo['thumb'] = json_decode($categoryInfo['thumb'], true);
        $goods = Goods::query()->whereIn('goods_id', explode(',', $categoryInfo["goods_ids"]))->get();
        foreach($goods as &$item){
            $item['goods_thumb'] = Arr::first(array_column(json_decode($item['goods_thumb'], true), 'url'));
        }
        $categoryInfo['goods'] = $goods;
        return $categoryInfo;
    }

    /**
     * @param $params
     * @param $categoryId
     * @return bool|int
     */
    public function update($params, $categoryId)
    {
        $lockName = 'category_update_' . auth('api')->id();
        $lock = \Cache::lock($lockName, 5);
        abort_if(!$lock->get(), HttpCode::TOO_MANY_REQUESTS, '请勿重复操作！');
        $data = array(
            'parent_id' => $params['parent_id'] ?? 0,
            'name' => $params['name'],
            'status' => $params['status'] ?? 0,
            'thumb' => json_encode($params['thumb'], true),
        );
        return Comment::query()->where('id', $categoryId)->update($data);
    }

    /**
     * @param $categoryId
     * @return bool|mixed|null
     */
    public function destroy($categoryId)
    {
        $data = Comment::query()->where('id', $categoryId)->first();
        abort_if(empty($data), HttpCode::FORBIDDEN, '分类不存在！');
        return Comment::query()->where('id', $categoryId)->delete();
    }
}