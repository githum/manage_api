<?php
/*
 * @author: ChenGuangHui
 */
namespace App\Logic\Goods;

use App\Models\Store\CouponRule;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Store\Servers;
use Exception;
use Illuminate\Database\QueryException;
use App\Logic\CommLogic;

class ServersLogic extends CommLogic
{
    /**
     * @author ChenGuangHui
     * @dateTime 2023-03-06
     * @param [type] $params
     */
    public function lists($params)
    {
        $query = Servers::query()
            ->when(isset($params['status']), function (Builder $query) use ($params) {
                $query->where('status', $params['status']);
            })
            ->when(!empty($params['title']), function (Builder $query) use ($params) {
                $query->where('title', 'like', '%' . $params['title'] . '%');
            });

        if (!empty($sortData)) {
            $query = $query->orderBy($sortData['sortRow'], $sortData['sortType']);
        } else {
            $query = $query->orderBy('id', 'desc');
        }

        $data = $query->get()->toArray();
        return $data;
    }

    /**
     * @author ChenGuangHui
     * @dateTime 2023-03-06
     * @param array $where
     * @param array $pageData
     * @param array $sortData
     */
    public function index($where = [], $pageData = ['pageSize' => 20, 'pageNow' => 1], $sortData = [])
    {
        $query = Servers::query()
            ->when(isset($where['status']), function (Builder $query) use ($where) {
                $query->where('status', $where['status']);
            })
            ->when(!empty($params['title']), function (Builder $query) use ($params) {
                $query->where('title', 'like', '%' . $params['title'] . '%');
            });

        if (!empty($sortData)) {
            $query = $query->orderBy($sortData['sortRow'], $sortData['sortType']);
        } else {
            $query = $query->orderBy('id', 'desc');
        }
        $data = $query->paginate($pageData['pageSize'], ['*'], 'page', $pageData['pageNow'])->toArray();

        foreach ($data['data'] as &$item) {
        }

        return $data;
    }

    /**
     * @author ChenGuangHui
     * @dateTime 2023-03-06
     * @param [type] $params
     */
    public function store($params)
    {
        $lockName = 'servers_store_' . auth('api')->id();
        $lock = \Cache::lock($lockName, 5);
        abort_if(!$lock->get(), HttpCode::TOO_MANY_REQUESTS, '请勿重复操作！');
        \DB::beginTransaction();
        try {
            $data = array(
                'title' => $params['title'],
                'icon' => $params['icon'] ?? '',
                'status' => $params['status'] ?? 0,
                'content' => $params['content'] ?? ''
            );
            Servers::query()->create($data);
            \DB::commit();
        } catch (QueryException $exception) {
            \DB::rollBack();
            throw new Exception($exception->getMessage(), $exception->getCode());
        }
        return true;
    }

    /**
     * @author ChenGuangHui
     * @dateTime 2023-03-06
     * @param [type] $data
     * @param [type] $id
     */
    public function show($data, $id)
    {
        $data = Servers::query()->where('id', $id)->first();
        abort_if(empty($data), HttpCode::FORBIDDEN, '服务不存在！');
        return $data;
    }

    /**
     * @author ChenGuangHui
     * @dateTime 2023-03-06
     * @param [type] $params
     * @param [type] $id
     */
    public function update($params, $id)
    {
        $data = Servers::query()->where('id', $id)->first();
        abort_if(empty($data), HttpCode::FORBIDDEN, '服务不存在！');
        $result = Servers::query()->where('id', $id)->update($params);
        return $result;
    }

    /**
     * @author ChenGuangHui
     * @dateTime 2023-03-06
     * @param [type] $id
     */
    public function destroy($id)
    {
        $data = Servers::query()->where('id', $id)->first();
        abort_if(empty($data), HttpCode::FORBIDDEN, '服务不存在！');
        $result = Servers::query()->where('id', $id)->delete();
        return $result;
    }
}