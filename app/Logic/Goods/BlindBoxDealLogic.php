<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/5/13 16:52
 */

namespace App\Logic\Goods;


use App\Enums\HttpCode;
use App\Logic\CommLogic;
use Illuminate\Database\Eloquent\Builder;
use App\Models\BlindBoxDeal;
use Illuminate\Support\Arr;

class BlindBoxDealLogic extends CommLogic
{
    
    /**
     * @param $params
     * @return mixed
     */
    public function lists($params)
    {
        $query = BlindBoxDeal::query()
            ->when(isset($params['status']), function (Builder $query) use ($params) {
                $query->where('status', $params['status']);
            })
            ->when(!empty($params['name']), function (Builder $query) use ($params) {
                $query->where('title', 'like', '%' . $params['name'] . '%');
            });

        if (!empty($sortData)) {
            $query = $query->orderBy($sortData['sortRow'], $sortData['sortType']);
        }else{
            $query = $query->orderBy('id', 'desc');
        }

        return $query->get()->toArray();
    }

    /**
     * @param array $where
     * @param array $pageData
     * @param array $sortData
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($where = [], $pageData = ['pageSize' => 20, 'pageNow' => 1], $sortData = [])
    {
        $query = BlindBoxDeal::query()
            ->when(isset($where['status']), function (Builder $query) use ($where) {
                $query->where('status', $where['status']);
            })
            ->when(!empty($where['name']), function (Builder $query) use ($where) {
                $query->where('title', 'like', '%' . $where['name'] . '%');
            });

        if (!empty($sortData)) {
            $query = $query->orderBy($sortData['sortRow'], $sortData['sortType']);
        }else{
            $query = $query->orderBy('id', 'desc');
        }
        $data = $query->paginate($pageData['pageSize'], ['*'], 'page', $pageData['pageNow'])->toArray();
        return $data;
    }

    /**
     * @param $params
     * @return \App\Models\Model|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function store($params)
    {
        $lockName = 'blind_box_store_' . auth('api')->id();
        $lock = \Cache::lock($lockName, 5);
        abort_if(!$lock->get(), HttpCode::TOO_MANY_REQUESTS, '请勿重复操作！');
        $data = array(
            'title' => $params['title'],
            'status' => $params['status'] ? 1 : 0,
            'color' => $params['color'],
            'reality_rate' => $params['reality_rate'],
            'virtual_rate' => $params['virtual_rate'],
            'remark' => $params['remark'] ?? '',
        );
        return BlindBoxDeal::query()->create($data);
    }

    /**
     * @param $blindBoxId
     * @return \App\Models\Model|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function show($data, $blind_boxId)
    {
        $data = BlindBoxDeal::query()->where('id', $blind_boxId)->first();
        abort_if(empty($data), HttpCode::FORBIDDEN, '盲盒不存在！');
        $blind_boxInfo = $data->toArray();
        return $blind_boxInfo;
    }

    /**
     * @param $params
     * @param $blind_boxId
     * @return bool|int
     */
    public function update($params, $blind_boxId)
    {
        $lockName = 'blind_box_update_' . auth('api')->id();
        $lock = \Cache::lock($lockName, 5);
        abort_if(!$lock->get(), HttpCode::TOO_MANY_REQUESTS, '请勿重复操作！');
        \DB::beginTransaction();
        $data = array(
            'title' => $params['title'],
            'status' => $params['status'] ? 1 : 0,
            'color' => $params['color'],
            'reality_rate' => $params['reality_rate'],
            'virtual_rate' => $params['virtual_rate'],
            'remark' => $params['remark'] ?? '',
        );
        return BlindBoxDeal::query()->where('id', $blind_boxId)->update($data);
    }

    /**
     * @param $blind_boxId
     * @return bool|mixed|null
     */
    public function destroy($blind_boxId)
    {
        $data = BlindBoxDeal::query()->where('id', $blind_boxId)->first();
        abort_if(empty($data), HttpCode::FORBIDDEN, '盲盒不存在！');
        return BlindBoxDeal::query()->where('id', $blind_boxId)->delete();
    }
}
