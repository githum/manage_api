<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/4/30 11:35
 */


namespace App\Logic\Advert;


use App\Enums\HttpCode;
use App\Logic\CommLogic;
use App\Models\Advert\Advert;
use App\Models\Store\User;
use Arr;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\QueryException;

class AdvertLogic extends CommLogic
{

    /**
     * @param array $where
     * @param array $pageData
     * @param array $sortData
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($where = [], $pageData = ['pageSize' => 20, 'pageNow' => 1], $sortData = [])
    {
        $query = Advert::query()
            ->when(isset($where['status']), function (Builder $query) use ($where) {
                $query->where('status', $where['status']);
            })
            ->when(!empty($where['name']), function (Builder $query) use ($where) {
                $query->where('name', '%' . $where['name'] . '%');
            });

        if (!empty($sortData)) {
            $query = $query->orderBy($sortData['sortRow'], $sortData['sortType']);
        }else{
            $query = $query->orderBy('id', 'desc');
        }

        $data = $query->paginate($pageData['pageSize'], ['*'], 'page', $pageData['pageNow'])
            ->toArray();

        foreach($data['data'] as &$item){
            $item['thumb'] = Arr::first(array_column(json_decode($item['thumb'], true), 'url'));
        }

        return $data;
    }

    /**
     * @param $params
     * @return bool
     * @throws \Throwable
     */
    public function store($params)
    {
        $lockName = 'tenant_store_' . auth('api')->id();
        $lock = \Cache::lock($lockName, 5);
        abort_if(!$lock->get(), HttpCode::TOO_MANY_REQUESTS, '请勿重复操作！');
        \DB::beginTransaction();
        try {
            list($valid_start, $valid_end) = $params['valid_time'];
            $data = array(
                'name' => $params['name'],
                'status' => $params['status'],
                'end_time' => $valid_end,
                'start_time' => $valid_start,
                'thumb' => json_encode($params['thumb'], true),
                'type' => $params['type'] ?? 1,
                'jump_url' => $params['jump_url'],
                'bgcolor' => $params['bgcolor'] ?? '',
                'module' => $params['module'],
                'sort' => $params['sort'] ?? 0,
                'remark' => $params['remark'] ?? '',
            );
            Advert::query()->create($data);
            \DB::commit();
        } catch (QueryException $exception) {
            \DB::rollBack();
            throw new \Exception($exception->getMessage(), $exception->getCode());
        }
        return true;
    }

    /**
     * @param $params
     * @param $id
     * @return Advert|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function show($params, $id)
    {
        $tenant = Advert::query()->where('id', $id)->first();
        abort_if(empty($tenant), HttpCode::FORBIDDEN, '找不到广告');
        $tenant->thumb = json_decode($tenant->thumb, true);
        $tenant->valid_time = array($tenant->start_time, $tenant->end_time);
        return $tenant;
    }

    /**
     * @param $params
     * @param $id
     * @return bool
     * @throws \Throwable
     */
    public function update($params, $id)
    {

        $lockName = 'tenant_update_' . auth('api')->id();
        $lock = \Cache::lock($lockName, 5);
        abort_if(!$lock->get(), HttpCode::TOO_MANY_REQUESTS, '请勿重复操作！');

        $tenant = Advert::query()->where('id', $id)->first();
        abort_if(empty($tenant), HttpCode::FORBIDDEN, '找不到广告');
        \DB::beginTransaction();
        try {
            list($valid_start, $valid_end) = $params['valid_time'];
            $data = array(
                'name' => $params['name'],
                'status' => $params['status'],
                'end_time' => $valid_end,
                'start_time' => $valid_start,
                'thumb' => json_encode($params['thumb'], true),
                'type' => $params['type'] ?? 1,
                'jump_url' => $params['jump_url'],
                'bgcolor' => $params['bgcolor'] ?? '',
                'module' => $params['module'],
                'sort' => $params['sort'] ?? 0,
                'remark' => $params['remark'] ?? '',
            );
            $tenant->update($data);
            \DB::commit();
        } catch (QueryException $exception) {
            \DB::rollBack();
            throw new \Exception($exception->getMessage(), $exception->getCode());
        }
        return true;
    }

    /**
     * @param $id
     * @return bool
     * @throws \Throwable
     */
    public function destroy($id)
    {

        $data = Advert::query()->where('id', $id)->first();
        abort_if(empty($data), HttpCode::FORBIDDEN, '找不到广告');
        return  $data->delete() ? true : false;
    }
}
