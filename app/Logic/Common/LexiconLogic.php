<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/7/13 13:53
 */


namespace App\Logic\Common;


use App\Enums\HttpCode;
use App\Logic\CommLogic;
use App\Models\Store\Lexicon;
use Illuminate\Database\Query\Builder;

class LexiconLogic extends CommLogic
{

    /**
     * @param $params
     * @return \App\Models\Model[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|mixed[]
     */
    public function lists($params)
    {
        return Lexicon::query()
            ->where('status', 1)
            ->whereIn('key', explode(',', $params['key']))
            ->when(!empty($where['name']), function (Builder $query) use ($params) {
                $query->where('name', 'like', '%' . $params['name'] . '%');
            })->get()
            ->groupBy('key')->toArray();
    }

    /**
     * @param array $where
     * @param array $pageData
     * @param array $sortData
     * @return mixed
     */
    public function index($where = [], $pageData = ['pageSize' => 20, 'pageNow' => 1], $sortData = [])
    {
        $query = Lexicon::query()
            ->when(isset($where['status']), function (Builder $query) use ($where) {
                $query->where('status', $where['status']);
            })
            ->when(!empty($where['name']), function (Builder $query) use ($where) {
                $query->where('name', 'like', '%' . $where['name'] . '%');
            })
            ->when(!empty($where['key']), function (Builder $query) use ($where) {
                $query->where('key', 'like', '%' . $where['key'] . '%');
            })
            ->when(!empty($where['value']), function (Builder $query) use ($where) {
                $query->where('value', 'like', '%' . $where['value'] . '%');
            });

        if (!empty($sortData)) {
            $query = $query->orderBy($sortData['sortRow'], $sortData['sortType']);
        }

        return $query->paginate($pageData['pageSize'], ['*'], 'page', $pageData['pageNow'])
            ->toArray();
    }

    /**
     * @param $params
     * @return Lexicon|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function store($params)
    {
        $lockName = 'lexicon_store_' . auth('api')->id();
        $lock = \Cache::lock($lockName, 5);
        abort_if(!$lock->get(), HttpCode::TOO_MANY_REQUESTS, '请勿重复操作！');
        $lexicon = Lexicon::query()->where('key', $params['key'])->first();
        abort_if(!empty($lexicon), HttpCode::FORBIDDEN, '字典已存在！');
        $data = array(
            'name' => $params['name'],
            'status' => $params['status'] ? 1 : 0,
            'key' => $params['key'],
            'sort' => $params['sort'],
            'value' => $params['value'],
        );
        return Lexicon::query()->create($data);
    }

    /**
     * @param $id
     * @return \App\Models\Model|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function show($id)
    {
        $data = Lexicon::query()->where('id', $id)->first();
        abort_if(empty($data), HttpCode::FORBIDDEN, '字典不存在！');
        return $data;
    }

    /**
     * @param $params
     * @param $id
     * @return bool|int
     */
    public function update($params, $id)
    {
        $lockName = 'lexicon_update_' . auth('api')->id();
        $lock = \Cache::lock($lockName, 5);
        abort_if(!$lock->get(), HttpCode::TOO_MANY_REQUESTS, '请勿重复操作！');

        $lexicon = Lexicon::query()->where('id', $id)->first();
        abort_if(empty($lexicon), HttpCode::FORBIDDEN, '字典不存在！');
        $data = array(
            'name' => $params['name'],
            'status' => $params['status'] ? 1 : 0,
            'sort' => $params['sort'],
            'value' => $params['value'],
        );
        return Lexicon::query()->where('id', $id)->update($data);
    }


    /**
     * @param $id
     * @return bool|mixed|null
     */
    public function destroy($id)
    {
        $data = Lexicon::query()->where('id', $id)->first();
        abort_if(empty($data), HttpCode::FORBIDDEN, '字典不存在！');
        return Lexicon::query()->where('id', $id)->delete();
    }
}
