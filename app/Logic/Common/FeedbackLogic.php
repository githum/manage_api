<?php
/*
 * @author: ChenGuangHui
 */
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/9/6 16:06
 */


namespace App\Logic\Common;


use App\Enums\HttpCode;
use App\Logic\CommLogic;
use App\Models\Common\Feedback;
use App\Models\Staffer\Staffer;
use Illuminate\Database\Query\Builder;

class FeedbackLogic extends CommLogic
{
    /**
     * @param array $where
     * @param array $pageData
     * @param array $sortData
     * @return mixed
     */
    public function index($where = [], $pageData = ['pageSize' => 20, 'pageNow' => 1], $sortData = [])
    {
        $query = Feedback::query()
            ->when(isset($where['type']), function (Builder $query) use ($where) {
                $query->where('type', $where['type']);
            })
            ->when(!empty($where['title']), function (Builder $query) use ($where) {
                $query->where('title', '%' . $where['title'] . '%');
            });

        if (!empty($sortData)) {
            $query = $query->orderBy($sortData['sortRow'], $sortData['sortType']);
        }

        $data = $query->paginate($pageData['pageSize'], ['*'], 'page', $pageData['pageNow'])
            ->toArray();

        $staffer = Staffer::query()->whereIn('id', array_column($data['data'], 'staffer_id'))->get()->keyBy('id')->toArray();
        foreach ($data['data'] as &$datum) {
            $datum['staffer_name'] = isset($staffer[$datum['staffer_id']]) ? $staffer[$datum['staffer_id']]['name'] : '';
        }

        return $data;
    }

    /**
     * @param $params
     * @return bool
     */
    public function store($params)
    {
        $lockName = 'bug_store_' . auth('api')->id();
        $lock = \Cache::lock($lockName, 5);
        abort_if(!$lock->get(), HttpCode::TOO_MANY_REQUESTS, '请勿重复操作！');
        $data = array(
            'staffer_id' => auth('api')->id(),
            'type' => $params['type'],
            'title' => $params['title'],
            'status' => $params['status'],
            'annex' => $params['annex'],
            'md_content' => $params['md_content'],
            'content' => $params['content'],
        );
        return Feedback::query()->insert($data);
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function show($id)
    {
        $data = Feedback::query()->where('id', $id)->first();
        abort_if(empty($data), HttpCode::TOO_MANY_REQUESTS, '反馈不存在！');
        return $data;
    }

    /**
     * @param $params
     * @param $id
     * @return bool|int
     */
    public function update($params, $id)
    {
        $lockName = 'bug_update_' . auth('api')->id();
        $lock = \Cache::lock($lockName, 5);
        abort_if(!$lock->get(), HttpCode::TOO_MANY_REQUESTS, '请勿重复操作！');

        $data = Feedback::query()->where('id', $id)->first();
        abort_if(empty($data), HttpCode::FORBIDDEN, '反馈不存在！');
        return Feedback::query()->where('id', $id)->update(array('status' => $params['status']));
    }

    /**
     * @param $id
     * @return bool|mixed|null
     */
    public function destroy($id)
    {
        $data = Feedback::query()->where('id', $id)->first();
        abort_if(empty($data), HttpCode::FORBIDDEN, '反馈不存在！');
        return Feedback::query()->where('id', $id)->delete();
    }
}
