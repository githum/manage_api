<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/7/13 13:53
 */


namespace App\Logic\Common;


use App\Enums\HttpCode;
use App\Logic\CommLogic;
use App\Models\Store\Help;
use Illuminate\Database\Query\Builder;

class HelpLogic extends CommLogic
{

    /**
     * @param $params
     * @return \App\Models\Model[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|mixed[]
     */
    public function lists($params)
    {
        return Help::query()
            ->when(!empty($where['name']), function (Builder $query) use ($params) {
                $query->where('name', 'like', '%' . $params['name'] . '%');
            })->get();
    }

    /**
     * @param array $where
     * @param array $pageData
     * @param array $sortData
     * @return mixed
     */
    public function index($where = [], $pageData = ['pageSize' => 20, 'pageNow' => 1], $sortData = [])
    {
        $query = Help::query()
            ->with('children')->where('parent_id', 0)
            ->when(isset($where['status']), function (Builder $query) use ($where) {
                $query->where('status', $where['status']);
            })
            ->when(!empty($where['name']), function (Builder $query) use ($where) {
                $query->where('name', 'like', '%' . $where['name'] . '%');
            });

        if (!empty($sortData)) {
            $query = $query->orderBy($sortData['sortRow'], $sortData['sortType']);
        } else {
            $query = $query->orderBy('id', 'desc');
        }

        return $query->paginate($pageData['pageSize'], ['*'], 'page', $pageData['pageNow'])
            ->toArray();
    }

    /**
     * @param $params
     * @return Help|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function store($params)
    {
        $lockName = 'help_store_' . auth('api')->id();
        $lock = \Cache::lock($lockName, 5);
        abort_if(!$lock->get(), HttpCode::TOO_MANY_REQUESTS, '请勿重复操作！');
        $help = Help::query()->where('key', $params['key'])->first();
        abort_if(!empty($help), HttpCode::FORBIDDEN, '帮助已存在！');
        $data = array(
            'title' => $params['title'],
            'status' => $params['status'] ? 1 : 0,
            'sort' => $params['sort'],
            'description' => $params['description'],
            'is_show' => $params['is_show'],
            'parent_id' => $params['parent_id'] ?? 0,
        );
        return Help::query()->create($data);
    }

    /**
     * @param $id
     * @return \App\Models\Model|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function show($id)
    {
        $data = Help::query()->where('id', $id)->first();
        abort_if(empty($data), HttpCode::FORBIDDEN, '帮助不存在！');
        return $data;
    }

    /**
     * @param $params
     * @param $id
     * @return bool|int
     */
    public function update($params, $id)
    {
        $lockName = 'help_update_' . auth('api')->id();
        $lock = \Cache::lock($lockName, 5);
        abort_if(!$lock->get(), HttpCode::TOO_MANY_REQUESTS, '请勿重复操作！');

        $help = Help::query()->where('id', $id)->first();
        abort_if(empty($help), HttpCode::FORBIDDEN, '帮助不存在！');
        $data = array(
            'title' => $params['title'],
            'status' => $params['status'] ? 1 : 0,
            'sort' => $params['sort'],
            'description' => $params['description'],
            'is_show' => $params['is_show'],
            'parent_id' => $params['parent_id'] ?? 0,
        );
        return Help::query()->where('id', $id)->update($data);
    }


    /**
     * @param $id
     * @return bool|mixed|null
     */
    public function destroy($id)
    {
        $data = Help::query()->where('id', $id)->first();
        abort_if(empty($data), HttpCode::FORBIDDEN, '帮助不存在！');
        return Help::query()->where('id', $id)->delete();
    }
}
