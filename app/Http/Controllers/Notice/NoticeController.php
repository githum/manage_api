<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/7/15 17:56
 */


namespace App\Http\Controllers\Notice;


use App\Http\Controllers\Controller;
use App\Logic\Notice\NoticeLogic;
use Illuminate\Http\Request;

class NoticeController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function lists(Request $request)
    {
        $list = (new NoticeLogic())->lists($request->all());
        return data($list);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $data = (new NoticeLogic())->index($request->all());
        return data($data);
    }

    /***
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $data = (new NoticeLogic())->show($id);
        return data($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\RJsonError
     * @throws \Exception
     * @throws \Throwable
     */
    public function store(Request $request)
    {
        $this->validate($request->all(), [
            'title' => 'required|string',
            'content' => 'required|string',
            'class_id' => 'required|integer'
        ]);
        if ($notice = (new NoticeLogic())->store($request->all())) {
            return data($notice);
        }
        return error();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancel(Request $request, $id)
    {
        if ((new NoticeLogic())->cancel($request->all(), $id)) {
            return success();
        }
        return error();
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function rollback(Request $request, $id)
    {
        if ((new NoticeLogic())->rollback($request->all(), $id)) {
            return success();
        }
        return error();
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function destroy(Request $request, $id)
    {
        return (new NoticeLogic())->destroy($id) ? success() : error();
    }

}
