<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/9/6 17:05
 */


namespace App\Http\Controllers\Notice;


use App\Http\Controllers\Controller;
use App\Logic\Notice\NoticeClassLogic;
use Illuminate\Http\Request;

class NoticeClassController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $data = (new NoticeClassLogic())->lists($request->all());
        return data($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\RJsonError
     */
    public function store(Request $request)
    {
        $this->validate($request->all(), [
            'class_name' => 'required|string',
        ]);

        if ($data = (new NoticeClassLogic())->store($request->all())) {
            return data($data);
        }
        return error();
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\RJsonError
     */
    public function update(Request $request, $id)
    {
        $this->validate($request->all(), [
            'class_name' => 'required|string',
        ]);

        if ((new NoticeClassLogic())->update($request->all(), $id)) {
            return success();
        }
        return error();
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        return (new NoticeClassLogic())->destroy($id) ? success() : error();
    }
}
