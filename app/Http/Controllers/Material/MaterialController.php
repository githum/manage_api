<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/7/22 14:10
 */


namespace App\Http\Controllers\Material;


use App\Http\Controllers\Controller;
use App\Logic\Material\MaterialLogic;
use Illuminate\Http\Request;

class MaterialController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function tree(Request $request)
    {
        $data = (new MaterialLogic())->tree();
        return data($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\RJsonError
     */
    public function index(Request $request)
    {
        $this->validate($request->all(), [
            'dir_id' => 'required|integer',
        ]);
        $list = (new MaterialLogic())->lists($request->all());
        return data($list);
    }

    /**
     * 创建文件夹
     * 
     * @author ChenGuangHui
     * @dateTime 2022-06-25
     * @param Request $request
     */
    public function dir(Request $request)
    {
        $this->validate($request->all(), [
            'name' => 'required|string',
        ]);
        $refult = (new MaterialLogic())->dir($request->all());
        return data($refult);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\RJsonError
     */
    public function store(Request $request)
    {
        $this->validate($request->all(), [
            // 'name' => 'required_without:file|string',
            // 'file' => 'required_without:name|file',
        ]);
        $refult = (new MaterialLogic())->store($request);
        return data($refult);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\RJsonError
     */
    public function update(Request $request, $id)
    {
        $this->validate($request->all(), [
            'name' => 'required_without:type|string',
            'type' => 'required_without:name|integer',
        ]);
        if ((new MaterialLogic())->update($request->all(), $id)) {
            return success();
        }
        return error();
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\RJsonError
     */
    public function destroy(Request $request, $id)
    {
        $this->validate($request->all(), [
            'type' => 'required|integer',
        ]);

        if ((new MaterialLogic())->destroy($request->all(), $id)) {
            return success();
        }
        return error();
    }

    /**
     * 文件下载
     *
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     * @throws \App\Exceptions\RJsonError
     */
    public function dowload(Request $request, $id)
    {
        $this->validate($request->all(), [
            'type' => 'required|integer',
        ]);

        return (new MaterialLogic())->dowload($request->all(), $id);
    }
}

