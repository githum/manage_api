<?php

namespace App\Http\Controllers\Order;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Logic\Order\BlindBoxOrderLogic;

class BlindBoxController extends Controller
{
    /**
     * 盲盒列表
     *
     * @param Request $request
     * @return array
     */
    public function index(Request $request)
    {
        $pageData = lists_page($request->all());
        $data = (new BlindBoxOrderLogic())->index($request->all(), $pageData);
        return lists($data);
    }
    
    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $data = (new BlindBoxOrderLogic())->show($request->all(), $id);
        return data($data);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        return (new BlindBoxOrderLogic())->destroy($id) ? success() : error();
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function ship(Request $request, $id)
    {
        return (new BlindBoxOrderLogic())->ship($id) ? success() : error();
    }
}