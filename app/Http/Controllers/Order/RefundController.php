<?php
/*
 * @author: ChenGuangHui
 */
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2022/04/13 16:52
 */

namespace App\Http\Controllers\Order;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Logic\Order\RefundLogic;

class RefundController extends Controller
{
    /**
     * Undocumented function
     *
     * @param Request $request
     * @return array
     */
    public function index(Request $request)
    {
        $pageData = lists_page($request->all());
        $data = (new RefundLogic())->index($request->all(), $pageData);
        return lists($data);
    }
    
    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $data = (new RefundLogic())->show($request->all(), $id);
        return data($data);
    }

    /**
     * @author ChenGuangHui
     * @dateTime 2022-06-14
     * @param Request $request
     */
    public function refund(Request $request)
    {
        $data = (new RefundLogic())->refund($request->all());
        return data($data);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        return (new RefundLogic())->destroy($id) ? success() : error();
    }
}