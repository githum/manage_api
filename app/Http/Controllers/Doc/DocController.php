<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/5/13 16:52
 */

namespace App\Http\Controllers\Doc;


use App\Http\Controllers\Controller;
use App\Logic\Doc\DocLogic;
use Illuminate\Http\Request;

class DocController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $data = (new DocLogic())->lists($request->all());
        return data($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\RJsonError
     */
    public function store(Request $request)
    {
        $this->validate($request->all(), [
            'title' => 'required|string',
            'class_id' => 'required|integer',
            'content' => 'required|string',
            'md_content' => 'required|string',
        ]);

        if ($data = (new DocLogic())->store($request->all())) {
            return data($data);
        }
        return error();
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $data = (new DocLogic())->show($id);
        return data($data);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\RJsonError
     */
    public function update(Request $request, $id)
    {
        // $this->validate($request->all(), [
        //     'type' => 'required_without:title|integer',
        //     'title' => 'required_without:type|string',
        // ]);

        if ((new DocLogic())->update($request->all(), $id)) {
            return success();
        }
        return error();
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function asterisk(Request $request, $id)
    {
        return (new DocLogic())->asterisk($id) ? success() : error();
    }


    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        return (new DocLogic())->destroy($id) ? success() : error();
    }
}
