<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/5/13 16:52
 */

namespace App\Http\Controllers\Doc;


use App\Http\Controllers\Controller;
use App\Logic\Doc\DocClassLogic;
use Illuminate\Http\Request;

class DocClassController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $data = (new DocClassLogic())->lists($request->all());
        return data($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\RJsonError
     */
    public function store(Request $request)
    {
        $this->validate($request->all(), [
            'class_name' => 'required|string',
        ]);

        if ($data = (new DocClassLogic())->store($request->all())) {
            return data($data);
        }

        return error();
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\RJsonError
     */
    public function update(Request $request, $id)
    {
        $this->validate($request->all(), [
            'doc_id' => 'required_without:class_name|integer',
            'class_name' => 'required_without:doc_id|string',
        ]);

        if ((new DocClassLogic())->update($request->all(), $id)) {
            return success();
        }

        return error();
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        return (new DocClassLogic())->destroy($id) ? success() : error();
    }
}
