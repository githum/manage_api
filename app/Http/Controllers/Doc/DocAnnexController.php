<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/8/17 16:58
 */


namespace App\Http\Controllers\Doc;


use App\Http\Controllers\Controller;
use App\Logic\Doc\DocAnnexLogic;
use Illuminate\Http\Request;

class DocAnnexController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $data = (new DocAnnexLogic())->lists($request->all());
        return data($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\RJsonError
     */
    public function upload(Request $request)
    {
        $this->validate($request->all(), [
            'file' => 'required|file',
            'doc_id' => 'required|integer',
        ]);

        $annex = (new DocAnnexLogic())->upload($request);
        return data($annex);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        return (new DocAnnexLogic())->destroy($id) ? success() : error();
    }
}
