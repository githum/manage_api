<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/7/15 17:44
 */


namespace App\Http\Controllers\Common;


use App\Http\Controllers\Controller;
use App\Logic\Common\BaseLogic;
use Illuminate\Http\Request;

class BaseController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function lists(Request $request)
    {
        $data = (new BaseLogic())->lists();
        return data($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\RJsonError
     */
    public function upload(Request $request)
    {
        $this->validate($request->all(), [
            'file' => 'required',
        ]);
        $file = $request->file('file');
        $data = (new BaseLogic())->upload($file, array(
            'file_path' => 'base/tmp',
            'extension' => $file->extension(),
            'original_name' => $file->getClientOriginalName(),
        ));
        return data($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function log(Request $request)
    {
        $pageData = lists_page($request->all());
        $list = (new BaseLogic())->log($request->all(), $pageData);
        return lists($list);
    }
}
