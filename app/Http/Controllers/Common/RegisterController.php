<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/4/30 18:00
 */


namespace App\Http\Controllers\Common;


use App\Http\Controllers\Controller;
use App\Logic\Common\RegisterLogic;
use App\Services\VerifyService;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /**
     * 账号登录
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\RJsonError
     */
    public function login(Request $request)
    {
        $this->validate($request->all(), [
            'account' => 'required|string',
            'password' => 'required|string',
            'verifyGuid' => 'required|string',
        ]);

        $data = (new RegisterLogic())->login($request->all());
        return data($data);
    }

    /**
     * @author ChenGuangHui
     * @dateTime 2023-03-06
     * @param Request $request
     */
    public function fixPassW(Request $request)
    {
        $this->validate($request->all(), [
            'password' => 'required|string',
            'new_password' => 'required|string',
            'pre_password' => 'required|string',
        ]);

        $data = (new RegisterLogic())->fixPassW($request->all());
        return data($data);
    }

    /**
     * 扫码登录
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\RJsonError
     */
    public function dingtalk(Request $request)
    {
        $this->validate($request->all(), [

        ]);

        $data = (new RegisterLogic())->dingtalk($request->all());
        return data($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\RJsonError
     */
    public function callback(Request $request)
    {
        $this->validate($request->all(), [

        ]);

        $data = (new RegisterLogic())->callback($request->all());
        return data($data);
    }

    /**
     * 退出登录
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        // abort_if(!auth('api')->invalidate(), 402, '登录异常');
        auth('api')->logout();
        return success('退出成功！');
    }

    /**
     * 获取验证码
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\RJsonError
     */
    public function verifyGuid(Request $request)
    {
        $this->validate($request->all(), [
            'guidCode' => 'required|string',
        ]);
        $verifyGuid = VerifyService::getImgVerifyBase64($request->get('guidCode', 'login'));
        return data(compact('verifyGuid'));
    }
}
