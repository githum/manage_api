<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/7/19 10:31
 */


namespace App\Http\Controllers\Common;


use App\Http\Controllers\Controller;
use App\Logic\Common\MenuLogic;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class MenuController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function tree(Request $request)
    {
        $data = (new MenuLogic())->tree($request->all());
        return data($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $pageData = lists_page($request->all());
        $list = (new MenuLogic())->index($request->all(), $pageData);
        return lists($list);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\RJsonError
     * @throws \Throwable
     */
    public function store(Request $request)
    {
        if ($id = $request->get('id')) {
            $data = (new MenuLogic())->update($request->all(), $id);
        } else {
            $this->validate($request->all(), [
                'auth_code' => [
                    // 'required',
                    Rule::unique('rbac_menu')->where('auth_code', $request->get('auth_code'))
                ],
            ]);
            $data = (new MenuLogic())->store($request->all());
        }
        return $data ? success() : error();
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $data = (new MenuLogic())->show($id);
        return data($data);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function destroy(Request $request, $id)
    {
        return (new MenuLogic())->destroy($id) ? success() : error();
    }
}
