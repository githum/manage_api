<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/7/8 14:14
 */


namespace App\Http\Controllers\Common;


use App\Http\Controllers\Controller;
use App\Logic\Common\RoleLogic;
use App\Services\OaService\OaFactory;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $pageData = lists_page($request->all());
        $data = (new RoleLogic())->index($request->all(), $pageData);
        return lists($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function lists(Request $request)
    {
        $data = (new RoleLogic())->lists($request->all());
        return data($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\RJsonError
     */
    public function store(Request $request)
    {
        $this->validate($request->all(), [
            'name' => 'required|string',
        ]);

        if ($id = $request->get('id')) {
            $data = (new RoleLogic())->update($request->all(), $id);
        } else {
            $data = (new RoleLogic())->store($request->all());
        }
        return $data ? success() : error();
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $data = (new RoleLogic())->show($request->all(), $id);
        return data($data);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        return (new RoleLogic())->destroy($id) ? success() : error();
    }
}
