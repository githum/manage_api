<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/7/13 13:54
 */


namespace App\Http\Controllers\Common;


use App\Http\Controllers\Controller;
use App\Logic\Common\LexiconLogic;
use Illuminate\Http\Request;

class LexiconController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\RJsonError
     */
    public function lists(Request $request)
    {
        $this->validate($request->all(), [
            'key' => 'required|string',
        ]);
        $data = (new LexiconLogic())->lists($request->all());
        return data($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $pageData = lists_page($request->all());
        $list = (new LexiconLogic())->index($request->all(), $pageData);
        return lists($list);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\RJsonError
     */
    public function store(Request $request)
    {
        $this->validate($request->all(), [
            'name' => 'required|string',
            'key' => 'required|string',
            'value' => 'required|string',
        ]);

        if ($id = $request->get('id')) {
            $data = (new LexiconLogic())->update($request->all(), $id);
        } else {
            $data = (new LexiconLogic())->store($request->all());
        }
        return $data ? success() : error();
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $data = (new LexiconLogic())->show($id);
        return data($data);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        return (new LexiconLogic())->destroy($id) ? success() : error();
    }
}
