<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/7/13 13:54
 */


namespace App\Http\Controllers\Common;


use App\Http\Controllers\Controller;
use App\Logic\Common\HelpLogic;
use Illuminate\Http\Request;

class HelpController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\RJsonError
     */
    public function lists(Request $request)
    {
        $this->validate($request->all(), [
            // 'key' => 'required|string',
        ]);
        $data = (new HelpLogic())->lists($request->all());
        return data($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $pageData = lists_page($request->all());
        $list = (new HelpLogic())->index($request->all(), $pageData);
        return lists($list);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\RJsonError
     */
    public function store(Request $request)
    {
        $this->validate($request->all(), [
            // 'title' => 'required|string',
        ]);

        if ($id = $request->get('id')) {
            $data = (new HelpLogic())->update($request->all(), $id);
        } else {
            $data = (new HelpLogic())->store($request->all());
        }
        return $data ? success() : error();
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $data = (new HelpLogic())->show($id);
        return data($data);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        return (new HelpLogic())->destroy($id) ? success() : error();
    }
}
