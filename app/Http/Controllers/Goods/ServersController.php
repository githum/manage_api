<?php
/*
 * @author: ChenGuangHui
 */

namespace App\Http\Controllers\Goods;

use Illuminate\Http\Request;
use App\Logic\Goods\ServersLogic;
use App\Http\Controllers\Controller;

class ServersController extends Controller
{
        /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function lists(Request $request)
    {
        $data = (new ServersLogic())->lists($request->all());
        return data($data);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return array
     */
    public function index(Request $request)
    {
        $pageData = lists_page($request->all());
        $data = (new ServersLogic())->index($request->all(), $pageData);
        return lists($data);
    }
    
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\RJsonError
     */
    public function store(Request $request)
    {
        $this->validate($request->all(), [
            // 'goods_name' => 'required',
        ]);

        if ($id = $request->get('id')) {
            $data = (new ServersLogic())->update($request->all(), $id);
        } else {
            $data = (new ServersLogic())->store($request->all());
        }
        return $data ? success() : error();
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $data = (new ServersLogic())->show($request->all(), $id);
        return data($data);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        return (new ServersLogic())->destroy($id) ? success() : error();
    }
}