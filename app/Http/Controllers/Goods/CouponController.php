<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2022/04/13 16:52
 */

namespace App\Http\Controllers\Goods;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Logic\Goods\CouponLogic;

class CouponController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function lists(Request $request)
    {
        $data = (new CouponLogic())->lists($request->all());
        return data($data);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return array
     */
    public function index(Request $request)
    {
        $pageData = lists_page($request->all());
        $data = (new CouponLogic())->index($request->all(), $pageData);
        return lists($data);
    }
    
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\RJsonError
     */
    public function store(Request $request)
    {
        $this->validate($request->all(), [
            // 'goods_name' => 'required',
        ]);

        if ($id = $request->get('id')) {
            $data = (new CouponLogic())->update($request->all(), $id);
        } else {
            $data = (new CouponLogic())->store($request->all());
        }
        return $data ? success() : error();
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $data = (new CouponLogic())->show($request->all(), $id);
        return data($data);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        return (new CouponLogic())->destroy($id) ? success() : error();
    }

    
    /**
     * 商品导入
     *
     * @param Request $request
     * @return void
     */
    public function import(Request $request)
    {
        return (new CouponLogic())->import($request->all()) ? success() : error();
    }
}