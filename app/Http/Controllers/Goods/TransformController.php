<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2022/04/13 16:52
 */

namespace App\Http\Controllers\Goods;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Logic\Goods\TransformLogic;
use App\Services\PddInterface;

class TransformController extends Controller
{

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function lists(Request $request)
    {
        $this->validate($request->all(), [
            'keyword' => 'required',
        ]);
        $pageData = lists_page($request->all());
        return (new TransformLogic())->lists($request->all(), $pageData);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function transform(Request $request)
    {
        $this->validate($request->all(), [
            'goods_sign' => 'required',
        ]);

        return (new TransformLogic())->transform($request->all());
    }
}