<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/4/30 11:24
 */

namespace App\Http\Controllers\User;


use App\Http\Controllers\Controller;
use App\Logic\User\UserLogic;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function lists(Request $request)
    {
        $data = (new UserLogic())->lists($request->all());
        return data($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $pageData = lists_page($request->all());
        $data = (new UserLogic())->index($request->all(), $pageData);
        return lists($data);
    }
    
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function coupon(Request $request)
    {
        $pageData = lists_page($request->all());
        $data = (new UserLogic())->coupon($request->all(), $pageData);
        return lists($data);
    }

    /**
     * @param Request $request
     * @param $uid
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $uid)
    {
        $data = (new UserLogic())->show($request->all(), $uid);
        return data($data);
    }

    /**
     * @param Request $request
     * @param $uid
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function destroy(Request $request, $uid)
    {
        return (new UserLogic())->destroy($uid) ? success() : error();
    }
}
