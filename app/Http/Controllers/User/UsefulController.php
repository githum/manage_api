<?php
/**
 * Author: ChenGuangHui
 * Email：13035809409@163.com
 * Date Time: 2021/4/30 11:24
 */


namespace App\Http\Controllers\User;


use App\Http\Controllers\Controller;
use App\Logic\User\UsefulLogic;
use Illuminate\Http\Request;

class UsefulController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function lists(Request $request)
    {
        $data = (new UsefulLogic())->lists($request->all());
        return data($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $pageData = lists_page($request->all());
        $data = (new UsefulLogic())->index($request->all(), $pageData);
        return lists($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\RJsonError
     * @throws \Throwable
     */
    public function store(Request $request)
    {
        $this->validate($request->all(), [
            // 'name' => [
            //     'required',
            //     Rule::unique('staffers')->where('name', $request->get('name'))
            // ],
            // 'avatar_url' => 'required|string'
        ]);

        if ($id = $request->get('id')) {
            $data = (new UsefulLogic())->update($request->all(), $id);
        } else {
            $data = (new UsefulLogic())->store($request->all());
        }

        return $data ? success() : error();
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $data = (new UsefulLogic())->show($request->all(), $id);
        return data($data);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function destroy(Request $request, $id)
    {
        return (new UsefulLogic())->destroy($id) ? success() : error();
    }
}
