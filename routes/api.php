<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'namespace' => 'Common'
], function () {
    // 首页展示台
    Route::group(['prefix' => 'home'], function () {
        Route::get('analyse', 'HomeController@analyse')->name('home.analyse');
    });
    // 公共模块
    Route::group(['prefix' => 'base'], function () {
        Route::post('upload', 'BaseController@upload')->name('base.upload');
        // 日志接口
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
        // 登录验证
        Route::post('login', 'RegisterController@login')->name('register.login');
        Route::get('logout', 'RegisterController@logout')->name('register.logout');
        Route::post('fixPassW', 'RegisterController@fixPassW')->name('register.fixPassW');
        Route::get('verifyGuid', 'RegisterController@verifyGuid')->name('register.verifyGuid');
    });

    // 权限菜单
    Route::group([
        'middleware' => ['auth.jwt', 'operation.log', 'tenant.init']
    ], function () {

        Route::get('log/lists', 'BaseController@log')->name('base.log');
        // 菜单管理
        Route::get('menu/tree', 'MenuController@tree')->name('menu.tree');
        Route::get('menu/lists', 'BaseController@lists')->name('base.menu');
        Route::apiResource('menu', 'MenuController')->only(['index', 'store', 'show', 'destroy']);
        // 角色管理
        Route::get('role/lists', 'RoleController@lists')->name('role.lists');
        Route::apiResource('role', 'RoleController')->only(['index', 'store', 'show', 'destroy']);
        Route::apiResource('feedback', 'FeedbackController')->only(['index', 'store', 'show', 'destroy']);

        // 配置
        Route::group(['prefix' => 'setting'], function () {
            Route::apiResource('lang', 'LangController');
            Route::get('lexicon/lists', 'LexiconController@lists')->name('lexicon.lists');
            Route::apiResource('lexicon', 'LexiconController')->only(['index', 'store', 'show', 'destroy']);
        });
    });

});

// Route::get('material/lists', 'Goods\TransformController@lists')->name('material.lists');
// Route::get('material/transform', 'Goods\TransformController@transform')->name('material.transform');

Route::group([
    'middleware' => ['auth.jwt', 'operation.log', 'tenant.init']
], function () {
    // 文档管理
    Route::get('doc/asterisk/{id}', 'Doc\DocController@asterisk')->name('doc.asterisk');
    Route::apiResource('doc/class', 'Doc\DocClassController');
    Route::post('doc/annex/upload', 'Doc\DocAnnexController@upload')->name('doc.upload');
    Route::apiResource('doc/annex', 'Doc\DocAnnexController')->only(['index', 'destroy']);
    Route::apiResource('doc', 'Doc\DocController');
    // 账号管理
    Route::get('staffer/lists', 'Staffer\StafferController@lists')->name('staffer.lists');
    Route::apiResource('staffer', 'Staffer\StafferController')->only(['index', 'store', 'show', 'destroy']);
    // 素材管理
    Route::get('material/dir', 'Material\MaterialController@dir')->name('material.dir');
    Route::get('material/tree', 'Material\MaterialController@tree')->name('material.tree');
    Route::get('material/dowload/{id}', 'Material\MaterialController@dowload')->name('material.dowload');
    Route::apiResource('material', 'Material\MaterialController')->only(['index', 'store', 'update', 'destroy']);

    Route::group(['namespace' => 'Notice'], function () {
        //笔记管理
        Route::get('notice/lists', 'NoticeController@lists')->name('notice.lists');
        Route::post('notice/cancel', 'NoticeController@cancel')->name('notice.cancel');
        Route::post('notice/rollback', 'NoticeController@rollback')->name('notice.rollback');
        Route::apiResource('notice/class', 'NoticeClassController');
        Route::apiResource('notice', 'NoticeController')->only(['index', 'store', 'show', 'destroy']);
    });

    // 帮助中心
    Route::get('help/lists', 'Common\HelpController@lists')->name('help.lists');
    Route::apiResource('help', 'Common\HelpController');

    // 商品
    Route::get('comment/lists', 'Goods\CommentController@lists')->name('comment.lists');
    Route::apiResource('comment', 'Goods\CommentController');
    
    Route::get('goods/lists', 'Goods\GoodsController@lists')->name('goods.lists');
    Route::apiResource('goods', 'Goods\GoodsController');
    
    //盲盒
    Route::get('blind_box/deal/lists', 'Goods\BlindBoxDealController@lists')->name('deal.lists');
    Route::apiResource('blind_box/deal', 'Goods\BlindBoxDealController');
    Route::get('blind_box/lists', 'Goods\BlindBoxController@lists')->name('blind_box.lists');
    Route::apiResource('blind_box', 'Goods\BlindBoxController');
    
    // 用户地址
    Route::get('address/lists', 'User\AddressController@lists')->name('address.level');
    Route::apiResource('address', 'User\AddressController');

    // 会员权益
    Route::get('member/level/lists', 'User\MemberController@lists')->name('member.level');
    Route::apiResource('member/level', 'User\MemberController');
    
    // 会员等级
    Route::get('member/useful/lists', 'User\UsefulController@lists')->name('member.useful');
    Route::apiResource('member/useful', 'User\UsefulController');
    
    // 运费
    Route::get('freight/lists', 'Goods\FreightController@lists')->name('freight.lists');
    Route::apiResource('freight', 'Goods\FreightController');

    // 分类
    Route::get('category/lists', 'Goods\CategoryController@lists')->name('category.lists');
    Route::get('category/import', 'Goods\CategoryController@import')->name('category.import');
    Route::apiResource('category', 'Goods\CategoryController');

    // 优惠券
    Route::get('coupon/lists', 'Goods\CouponController@lists')->name('coupon.lists');
    Route::get('coupon/import', 'Goods\CouponController@import')->name('coupon.import');
    Route::apiResource('coupon', 'Goods\CouponController');
    // 订单销售
    Route::apiResource('refund', 'Order\RefundController');
    Route::put('order/ship', 'Order\OrderController@ship')->name('order.ship');
    Route::get('order/analyze', 'Order\OrderController@analyze')->name('order.analyze');
    Route::apiResource('order', 'Order\OrderController');
    // 用户管理
    Route::get('user/lists', 'User\UserController@lists')->name('user.lists');
    Route::get('user/coupon', 'User\UserController@coupon')->name('user.coupon');
    Route::get('user/point', 'User\UserController@point')->name('user.point');
    Route::apiResource('user', 'User\UserController')->only(['index', 'show', 'destroy']);
    // 租户管理
    Route::get('tenant/lists', 'Tenant\TenantController@lists')->name('tenant.lists');
    Route::apiResource('tenant', 'Tenant\TenantController');
    // 广告管理
    Route::apiResource('advert', 'Advert\AdvertController');
    Route::apiResource('activity', 'Activity\ActivityController');
});
