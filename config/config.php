<?php
return [
    /*
    |--------------------------------------------------------------------------
    | 阿里云OSS配置文件
    |--------------------------------------------------------------------------
    |
    */
    'aliyun' => [
        'host_url' => env('ALIYUN_OSS_HOST_URL', 'http://localhost'),
        'bucket' => env('ALIYUN_OSS_BUCKET', null),
        'endpoint' => env('ALIYUN_OSS_ENDPOINT', null),
        'access_key' => env('ALIYUN_OSS_ACCESS_KEY_ID', null),
        'access_secret' => env('ALIYUN_OSS_SECRET_ACCESS_KEY', null),
    ]
];
